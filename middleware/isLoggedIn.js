// self-made middleware
// determine if request is sent from a member


module.exports = function() {

    return function(req, res, next) {
        // if request comes from someone logged in, continue operation
        if (req.session && req.session.user && req.session.user._id) {
            next();
        }
        // or else
        else {
            res.json({ success: false, error: 'You must be logged in.' });
        }

    }

};