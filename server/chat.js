var _ = require('underscore');
var Message = require('../models/message.js');




GLOBAL.G_User = {}; // match any online user's id to his data, data is { currentRoom: ____, socketId: ______, online: ____ } extended by his req.session.user
GLOBAL.G_Room = {}; // we want to save all rooms' conversations, this object matches room name with its data.
                    // On client side, we should always refer a room by its 'name' field, because server side access a room
                    // by G_Room[name].
                    // Each room inside it has structure:
                    // name: ____, (the key of this roomObj)
                    // original_name: _____, (the original name room creator gave)
                    // desc: ____,
                    // profile_pic: ____,
                    // participants: { <_id>: { username: ____, profile_pic: _____ }, etc },
                    // messages: [ { sender: ____, content: ____, userId: ____, profile_pic: ____, date: ____ }, etc ]



// ========================== GLOBAL API =============================

// record user to global G_User object, so that his data is accessible directly via his userId
GLOBAL.RecordUser = function(userId, session){
    G_User[userId] = G_User[userId] ? G_User[userId] : {};
    _.extend(G_User[userId], session.user);
};

// delete user from global Object
GLOBAL.DeleteUser = function(userId){
    delete G_User[userId];
};

// Set an attribute for user
GLOBAL.SetUser = function(userId, obj){
    if (!G_User[userId]){
        console.log('can not set attribute', obj, 'for user', userId);
        return;
    }
    _.extend(G_User[userId], obj);
};

// Get an attribute of user
GLOBAL.GetUser = function(userId, attr){
    if (!G_User[userId]) return false;
    return G_User[userId][attr];
};

// get socket id of an user
GLOBAL.GetSocketId = function(userId){
    if (!G_User[userId]) return false;
    return G_User[userId].socketId;
};

// set socket id for an user
GLOBAL.SetSocketId = function(userId, socketId){
    if (!G_User[userId]) return false;
    G_User[userId].socketId = socketId;

};




// ========================== Room API =============================

// fetch lists of all rooms
// it returns the G_Room object, except without the 'messages' field for each room because that would
// cause too much network traffic (i think)
function fetchAllRooms(){
    var rooms = {};
    _.each(G_Room, function(roomObj, name){
        rooms[name] = _.omit(roomObj, 'messages');
    });
    return rooms;
}


// create a room
// we should always refer a room by its 'name' field, because server side access a room by doing G_Room[name]
// name is the lower case version of original_name, this is to avoid having duplicate rooms with same lower cased word
function createRoom(roomObj, callback){
    var name = roomObj.original_name.toLowerCase();
    // if room already exist
    if (G_Room[name]){
        callback(false);
    }
    // if room doesn't exist, create it
    else {
        var defaultRoom = {
            original_name: roomObj.original_name, // this field is the original name that room creator gave
            name: name, // the key for roomObj is its lowercase in order to avoid duplicate while creating rooms
            desc: roomObj.desc,
            profile_pic: roomObj.profile_pic,
            participants: {}, // hashtable, each key value pair is: _id: { username: ___, profile_pic: ___ }
            messages: [] // array of jsons, each with fields: sender, content, sent
        };
        G_Room[name] = defaultRoom;
        callback(defaultRoom);
    }
}

// join a room
function joinRoom(room, userObj, callback){
    // if room exists
    if (G_Room[room]){
        // join the room
        G_Room[room].participants[userObj._id] = { username: userObj.username, profile_pic: userObj.profile_pic };
        // record it in his global object
        userObj.currentRoom = room;
        // success
        callback(true);
    }
    // if room doesn't exist
    else {
        callback(false);
    }

}

// fetch a room's data
function fetchRoom(room){
    return (G_Room[room]);
}

// leave a room
function leaveRoom(room, userObj, callback){
    if (G_Room[room] && G_User[userObj._id]){
        G_User[userObj._id].currentRoom = null;         // reset the currentRoom field of his global object
        delete G_Room[room].participants[userObj._id];  // delete him from room
        callback();
    }
    else {
        callback();
    }
}

// add message to room
function recordMessage(msgObj){
    if (!G_Room[msgObj.room]) return;
    G_Room[msgObj.room].messages.push(msgObj);
}

// every 30 min, we do a clean up to make sure G_Room has maximum 1000 messages in it
function cleanRooms(){
    _.each(G_Room, function(roomObj, room){
        if (_.size(roomObj.messages) <= 1000) return;
        roomObj.messages = _.last(roomObj.messages, 1000);
    });
}
setInterval(function(){ cleanRooms }, 900000);






// ========================== Helper functions =============================

function removeFromArray(arr, element){
    var index = arr.indexOf(element);
    if (index > -1) {
        arr.splice(index, 1);
    }
}





module.exports = function(app){


    sessionSockets.on('connection', function(err, socket, session){
        if (!session || !session.user || !session.user._id) return;

        var userId = session.user._id;

        // mark him as online
        SetUser(userId, { online: true });

        // save each user's socket ID in a global object
        SetSocketId(userId, socket.id);

        console.log('socket established connection')

        // fetch all rooms
        socket.on('fetch_all_rooms', function(){
            socket.emit('fetch_all_rooms', fetchAllRooms());
        });

        // join a room (this will create if room doesn't exist)
        socket.on('join', function(room){
            socket.join(room);
            joinRoom(room, session.user, function(success){
                if (success){
                    // fetch him the room data
                    socket.emit('join_success', fetchRoom(room));
                    // notify all others in room by fetching current containing participants
                    socket.broadcast.to(room).emit('new_joiner', G_Room[room].participants);
                }
            });

        });

        // leave a room
        socket.on('leave', function(room){
            leaveRoom(room, session.user, function(success){
                if (success){
                    socket.leave('room');
                    // notify all others in room by fetching current containing participants
                    socket.broadcast.to(room).emit('leaver', G_Room[room].participants);
                }
            });
        });

        // create a room
        socket.on('create', function(roomObj){
            createRoom(roomObj, function(newRoomObj){
                if (newRoomObj){
                    // make him room
                    joinRoom(newRoomObj.name, session.user, function(success){
                        if (success){
                            // fetch him the room data
                            socket.emit('create_success', fetchRoom(newRoomObj.name));
                        }
                    });
                }
                else {
                    socket.emit('create_fail', roomObj);
                }
            });
        });

        // send message to a room
        socket.on('room_message', function(msgObj){
            if (!msgObj.content || !msgObj.room) return; // safety. if room is empty, code will crash

            // recreate a new msgObj
            msgObj = _.pick(msgObj, ['content', 'room']);
            msgObj.userId = userId;

            // for username and profile_pic, we use the ones saved in G_User for most updated version, instead of socket session
            // ones which is created on connection. However if due to some reason it gives null, we'll use the session ones
            msgObj.sender = GetUser(msgObj.userId, 'username') || session.user.username;
            msgObj.profile_pic =  GetUser(msgObj.userId, 'profile_pic') || session.user.profile_pic;
            if (!msgObj.sender || !msgObj.profile_pic) return; // just to make sure
            msgObj.date = (new Date()).toISOString();

            io.sockets.in(msgObj.room).emit('room_message', msgObj);
            // record it in global room variable
            // !TODO: room field is useless, and profile_pic field is present in every message obj, hence will take memory
            // and second, it stores the profile_pic of user at the moment he sent the message, and not necessary the current latest one
            recordMessage(msgObj);
        });


        // send message to an individual (inbox)
        socket.on('personal_message', function(msgObj){
            if (!msgObj.content || !msgObj.convoId) return;

            // recreate a new msgObj
            msgObj = _.pick(msgObj, ['content', 'convoId']);
            msgObj.userId = userId;

            // for username and profile_pic, we use the ones saved in G_User for most updated version, instead of socket session
            // ones which is created on connection. However if due to some reason it gives null, we'll use the session ones
            msgObj.sender = GetUser(msgObj.userId, 'username') || session.user.username;
            msgObj.profile_pic =  GetUser(msgObj.userId, 'profile_pic') || session.user.profile_pic;
            if (!msgObj.sender || !msgObj.profile_pic) return; // just to make sure
            msgObj.date = (new Date()).toISOString();

            // insert message to database
            Message.update (userId, msgObj.convoId, msgObj.content, function (data) {
                if (data) {
                    var participants_arr = _.uniq(data.participants_arr);

                    // notify all conversation participants with socket
                    participants_arr.forEach(function(user){
                        if (GetUser(user, 'online')){
                            io.sockets.socket(GetSocketId(user)).emit('personal_message', msgObj);
                        }
                    });

                    // update conversation order for all participants in each of their user database respectively
                    Message.updateConvoOrder(data.participants_arr, msgObj.convoId);
                }
            });


        });



        socket.on('disconnect', function(){
            if (!G_User[userId]) return;

            // notify people in his room that he has left
            var currentRoom = G_User[userId].currentRoom;
            leaveRoom(currentRoom, session.user, function(success){
                if (success){
                    socket.broadcast.to(currentRoom).emit('leaver', G_Room[currentRoom].participants);
                }
            });

            // mark him as offline
            // Here's how offline-online mechanism work: when socket connects, it's going to set the user to online
            // then when socket disconects, it's going to set him to offline. Then after 10 secs, if he's still offline
            // we are going to remove him from global G_User object. If he does a refresh, then socket on connection event
            // will set his online attribute as true, then later when the setTimeout triggered, it'll see he's online, so
            // his G_User object will not be deleted.
            SetUser(userId, { online: false });

            // if he's offline for more than 10 seconds, delete him from global object
            // we do it twice to avoid accident (ex: he refresh again in 10s)
            // make sure its the current socket, cuz if its past socket, then we remove G_User, then its bad
            var really = false;
            setTimeout(function(){
                if (!GetUser(userId, 'online') && socket.id == GetSocketId(userId)){
                    really = true;
                }
            }, 3000);
            setTimeout(function(){
                if (!GetUser(userId, 'online') && socket.id == GetSocketId(userId) && really){
                    DeleteUser(userId);
                    console.log('user', userId, 'has disconnected');
                }
            }, 5000);

        });
    });


}




createRoom({ original_name: 'Mathematics', desc: "Are your angles less than 90 ? Because you're sure acute.", profile_pic: '/images/subject/mathematics.png' }, function(){});
createRoom({ original_name: 'Life', desc: 'Do you have a map? I just keep on getting lost in your eyes.', profile_pic: '/images/subject/life.png' }, function(){});
createRoom({ original_name: 'Physics', desc: "Was that an earthquake? Or did you just rock my world.", profile_pic: '/images/subject/physics.png' }, function(){});
createRoom({ original_name: 'Biology', desc: "My Love for you is like diarrahia... I can't hold it in.", profile_pic: '/images/subject/biology.png' }, function(){});
createRoom({ original_name: 'Computer Science', desc: "Is your name Google? Because you have everything I've been searching for.", profile_pic: '/images/subject/computerscience.png' }, function(){});


