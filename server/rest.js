// RESTful API ====================================
// !TODO: error response should also be json, aka res.json(), since it's a RESTful api


// loaded all we need
var mongoose = require('mongoose');		//Load mongoose database library
var User = require('../models/user.js');
var Photo = require('../models/photo.js');
var Profile = require('../models/profile.js');
var Message = require('../models/message.js');
var _ = require('underscore');
var isLoggedIn = require('../middleware/isLoggedIn.js');



// =========================== helper functions ===========================

function arrToStr(arr){
	return arr.sort().join(",");
};




module.exports = function(app){

	function landing(req, callback){
		// data must be consistent with userData (from user.js), and index.html's user model in ejs
	    var data = { title: 'Chatr', username: undefined, profile_pic: undefined,
	    status: undefined, photo: undefined, name: undefined, major: undefined,
	    school: undefined, interest: undefined, about_me: undefined,
	    view: undefined, _id: undefined, message: undefined };
	    if (req.session && req.session.user && req.session.user._id){ // if already logged in
	        data.guest = false; // directly go to chat
	        User.me(req.session.user._id, function(userData){
	        	if (userData) {
	        		_.extend(data, userData);
	        		// save updated user data into session
	        		_.extend(req.session.user, userData);
	        		req.session.save();
	        		// save user to a global variable
	        		RecordUser(userData._id, req.session);
	        		console.log('landed')
	        		callback(data);
	        	}
	        	else { // i dont think this condition will ever meet..
	        		callback(data);
	        	}
	        });
	    }
	    else { // if not
	        data.guest = true; // show login screen
	        callback(data);
	    }

	}

	app.get('/', function(req, res){
	    landing(req, function(data){
	        res.render('index', data);
	    });
	});

	app.get('/logout', isLoggedIn(), function(req, res){
        req.session.destroy(function(){
            res.redirect('/');
        });
    });

	app.post('/signup', function(req, res){
		var username = req.body.username;
		var password = req.body.password;
		if (!username || !password){  res.end(); return; }

		if (username.length < 3){
			res.end('Username should be at least 3 characters.');
			return;
		}
		if (!username || !password){
			res.end('Username or password missing.');
			return;
		}

		User.signup(username, password, function(userData){
			if (userData){
				req.session.user = { guest: false, currentRoom: null };
 				_.extend(req.session.user, userData);
 				req.session.save();
 				RecordUser(req.session.user._id, req.session); // save user to a global variable
 				var response = _.extend({ success: true }, req.session.user);
 				res.json(response);
			}
			else {
				res.json({ success: false, error: 'Username already taken.' });
			}
		});
	});


	app.post('/login', function(req, res){
		var username = req.body.username;
		var password = req.body.password;
		if (!username || !password){  res.end(); return; }
		username = username.toLowerCase();

		User.login(username, password, function(userData){
			if (userData){
				req.session.user = { guest: false, currentRoom: null };
 				_.extend(req.session.user, userData);
 				req.session.save();
 				RecordUser(req.session.user._id, req.session); // save user to a global variable
 				var response = _.extend({ success: true }, req.session.user);
 				res.json(response);
			}
			else {
				res.json({ success: false, error: 'Wrong username or password.' });
			}
		});
	});


	app.post('/profile', isLoggedIn(), function(req, res){
		Profile.edit(req.session.user._id, req.body, function(success){
			if (success){
				res.json({ success: true });
			} else {
				res.json({ success: false });
			}
		});
	});

	// fetch user's profile
	app.get('/profile/:id', function(req, res){
		var target = req.params.id;
		Profile.fetch(target, function(data){
			if (data){
				res.json(data);
			}
			else {
				res.end();
			}
		});
	});

	// fetch user's all conversations
	app.get ('/message', isLoggedIn(), function (req, res) {
		var userId = req.session.user._id;
		Message.fetchAll (userId, function (data) {
			if (data) {
				res.json(data);
			} else { res.end(); }
		});
	});

	// fetch a conversation by convoiD
	app.get ('/message/:convoId', isLoggedIn(), function (req, res) {
		Message.fetchById (req.params.convoId, req.session.user._id, function (data) {
			if (data) {
				res.json(data);
			} else { res.end(); }
		});
	});

	// fetch a conversation providing participants names
	// if not found, it will create a new conversation
	app.post('/message/fetch', isLoggedIn(), function (req, res){
		var participants = arrToStr(req.body.participants);
		var userId = req.session.user._id;

		if (participants.indexOf (userId) > -1) { // make sure requester is among of participants
			Message.fetchByParticipants (participants, userId, function (data) {
				if (data) {
					res.json(data);
				} else { res.end(); }
			});
		} else { res.end(); }
	});





}
