var mongoose = require('mongoose');		//Load mongoose database library
var User = require('../models/user.js').model;
var _ = require('underscore');


module.exports = function(app){


	// // login & signup using socket

	// sessionSockets.on('connection', function(err, socket, session){

	//     socket.on('signup', function(data){
	//         var username = data.username;
	// 		var password = data.password;
	//     });

	//     // if valid
	// 	if (username && password) {
	// 		if (username.length < 3){
	// 			res.end('Username should be at least 3 characters.');
	// 		}
	// 		else {
	// 			var user = new User({ username: username, password: password });
	// 			user.save(function(err, userData){
	// 				// need to add duplicate username/email check later...
	// 				if(err) {
	// 					console.log(err);
	// 					res.end('Username already taken.');
	// 				} else {
	// 	 				res.end('success');
	// 	 				req.session.user = { guest: false };
	// 	 				_.extend(req.session.user, userData);
	// 	 				console.log(req.session)
	// 				}
	// 			});
	// 		}
	// 	} else {
	// 		// if not valid
	// 		res.end('Invalid username or password.');
	// 	}

	// });






	app.post('/signup', function(req, res){

		var username = req.body.username;
		var password = req.body.password;

		// if valid
		if (username && password) {
			if (username.length < 3){
				res.end('Username should be at least 3 characters.');
			}
			else {
				var user = new User({ username: username, password: password });
				user.save(function(err, userData){
					// need to add duplicate username/email check later...
					if(err) {
						console.log(err);
						res.json({ success: false, error: 'Username already taken.' });
					} else {
		 				res.json({ success: true, username: username });
		 				req.session.user = { guest: false };
		 				_.extend(req.session.user, userData);
		 				req.session.save();
					}
				});
			}
		} else {
			// if not valid
			res.end('Invalid username or password.');
		}
	});


	app.post('/login', function(req, res){
		var username = req.body.username.toLowerCase();
		var password = req.body.password;

		User.findOne({
			username: username, // if it matches username or email
			password: password // and it matches password
		}, function (err, userData) {
			if (err) {
				console.error(err);
				res.json({ success: false, error: 'Username already taken.' });
			}
		 	// if found user
		 	if (userData){
		 		res.json({ success: true, username: username });
		 		req.session.user = { guest: false };
		 		_.extend(req.session.user, userData);
		 		req.session.save();
		 	}
		 	// if not found
		 	else {
		 		res.end('Oops, wrong username or password.');
		 	}
		})
	});




}
