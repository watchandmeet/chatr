// load up dependencies
var User = require('../../models/user.js');
var Notification = require('../../models/notification.js');
var _ = require('underscore');

// wrapper function for sending real-time notification to online user
var send = function(eventName, from, to, data){
	if (isOnline(to) && data.pic){
		io.of('/notification').socket(NameToID[to]).emit(eventName, data);
	}
};

var sendData = function(eventName, to, data){
	if (isOnline(to)){
		io.of('/notification').socket(NameToID[to]).emit(eventName, data);
	}
};


// stuff to export
module.exports = {

	friendRequest : function(friend, askerData){
		// askerData object structure is :
		// { username: username, level: level, profile_pic: profile_pic };
		if (isOnline(friend)){
			io.of('/notification').socket(NameToID[friend]).emit('friendRequest', askerData);
		}
	},

	message : function(to, from, message, profile_pic, time){
		var content = {
			from: from,
			message: message,
			profile_pic: profile_pic,
			time: time
		};
		if (isOnline(to)){
			io.of('/notification').socket(NameToID[to]).emit('message', content);
		}
	},

	notification : function(to, type, pic, time){
		var content = {
			pic: pic,
			time: time
		};
		if (isOnline(to)){
			io.of('/notification').socket(NameToID[to]).emit('notification', asker);
		}
	},

	goOnline : function(username){
		User.fetchCostum(username, User.publicFields.concat(['friend_list']), function(data){
			if (data){
				data.friend_list.forEach(function(friend){
					if (isOnline(friend)){
						io.of('/notification').socket(NameToID[friend]).emit('newOnline', _.omit(data, 'friend_list'));
					}
				});
			}
		});
	},

	goOffline : function(username){
		User.fetchCostum(username, User.publicFields.concat(['friend_list']), function(data){
			if (data){
				data.friend_list.forEach(function(friend){
					if (isOnline(friend)){
						io.of('/notification').socket(NameToID[friend]).emit('newOffline', _.omit(data, 'friend_list'));
					}
				});
			}
		});
	},

	newFriend : function(confirmer, requester){ // used for putting friend in left chatbox friendlist upon confirming friend request
		User.fetchPublic(confirmer, function(data){
			if (data){
				if (isOnline(requester)){ // if requester is online
					data.online = isOnline(confirmer); // add confirmer to his chat box with appropriate state (online or offline)
					io.of('/notification').socket(NameToID[requester]).emit('newFriend', data);
				}
			}
		});
		User.fetchPublic(requester, function(data){
			if (data){
				if (isOnline(confirmer)){ // if confirmer is online, which is very likely
					data.online = isOnline(requester); // add requester to his chat box with appropriate state (online or online)
					io.of('/notification').socket(NameToID[confirmer]).emit('newFriend', data);
				}
			}
		});
	},

	// on front-end, this automatically gives user both a dropdown notification and a
	// right-bottom corner's bubble notification
	notification: function(from, to, type, postId, whosePage){
		if (from != to){
			// default parameters
			var content = '';
			var link = ''; // which page does clicking that notification lead to
			// postId 		: upon landing on the page, it scroll down to element having this id
			// others[]		: other people to be notified via socket only

			// modify content for different type of notification
			switch (type) {

			    case 'like':
			        content = 'likes you.';
			        link = '/profile/' + from;
			        break;

			    case 'confirmFriend':
			        content = 'accepted your friend request.';
			        link = '/profile/' + from;
			        break;

			    case 'likePost':
			        content = 'likes your post.';
			        link = '/profile/' + whosePage + '/wall#' + postId;
			        break;

			    case 'likePostComment':
			        content = 'likes your comment on a post.';
			        link = '/profile/' + whosePage + '/wall#' + postId;
			        break;

				case 'likePhoto':
			        content = 'likes your photo.';
			        link = '/profile/' + whosePage + '/photos?id=' + postId;
			        break;

			    case 'likePhotoComment':
			        content = 'likes your comment on ' + whosePage + "'s photo.";
			        link = '/profile/' + whosePage + '/photos?id=' + postId;
			        break;

			    case 'wallPost':
			        content = 'wrote something on your wall.';
			        link = '/profile/' + to + '/wall#' + postId;
			        break;
			}

			// save to Notification database
			Notification.create(from, to, content, link, postId, type, function(id){
				if (id){
					// send socket
					User.fetchPublic(from, function(result){
						if (result){
							// currently, only the 'to' guy receives real-time update
							// !TODO: in the future, everyone who's on that person (whosepage)'s page
							// should receive updates
							send('notification', from, to, {
								id 			: id,
								pic 		: result.profile_pic,
								from 		: from,
								content 	: content,
								link 		: link,
								postId		: postId,
								whosePage	: whosePage,
								type		: type
							});

						}
					});
				}
			});
		}
	},


	// same as notification, but for 2 special cases (postCOmment and photoComment)
	notification2: function(from, participants, type, post, whosePage){

		participants.forEach(function(to){

			var content = '';
			var link = '';
			var postAuthor = post.username;
			var postId = post._id;

			var who = (postAuthor == to) ? 'your' : postAuthor + "'s";
			// put 'also' if there are > 1 comment AND it's not all from the same person
			var also = (post.comment.length > 1 && !_.isEmpty(_.without(_.pluck(post.comment, 'username'), post.comment[0].username))) ? 'also ' : '';

			switch (type) {

			    case 'postComment':
			    	content = also + 'commented on ' + who + ' post.';
			        link = '/profile/' + whosePage + '/wall#' + postId;
			        break;

			    case 'photoComment':
			        content = also + 'commented on ' + who + ' photo.';
			        link = '/profile/' + whosePage + '/photos?id=' + postId;
			        break;
			}

			Notification.create(from, to, content, link, postId, type, function(id){
				if (id){
					User.fetchPublic(from, function(result){
						if (result){
							send('notification', from, to, {
								id 			: id,
								pic 		: result.profile_pic,
								from 		: from,
								content 	: content,
								link 		: link,
								postId		: postId,
								whosePage	: whosePage,
								type		: type
							});
						}
					});
				}
			});
		});
	},

	// used in rest.js to fetch real-time data (ex: new wall post, comments, etc)
	sendData: function(to, type, postId, data){
		_.each(_.uniq(to), function(user){
			sendData('newData', user, {
				postId		: postId,
				type		: type,
				socketData	: data
			});
		});
	},

	// tell user that he must be logged in to performe such action (ex: add someone as friend)
	mustBeLoggedIn: function(username){
		sendData('mustBeLoggedIn', username);
	},

	// tell players their experience is increased, currently used after database write (see models/exp.js)
	addExp: function(username_arr, exp){
		_.each(username_arr, function(user){
			sendData('addExp', user, exp);
		});
	},

	// tell players their coins are changed, currently used after database write (see models/coin.js)
	addCoin: function(username_arr, amount){
		_.each(username_arr, function(user){
			sendData('addCoin', user, amount);
		});
	}




};

