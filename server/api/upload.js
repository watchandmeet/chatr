// this file handles the routing for photo upload on user's profile > photo page

// load up dependencies
var fs = require("fs");
var _ = require("underscore");
var multer  = require('multer');
var Photo = require('../../models/photo.js');
var isLoggedIn = require('../../middleware/isLoggedIn.js');


// --------------> helper functions <-------------- //
var acceptedFormats = ['png', 'jpg', 'jpeg', 'gif', 'bmp'];
var isValide = function(extension){
    return _.contains(acceptedFormats, extension);
};



module.exports = function(app){

    app.use(multer({ // check file type and upload
        dest: './public/photos',

        onFileUploadStart: function (file) {
            if (isValide(file.extension)) {
                file.url = '/photos/' + file.name;
            } else {
                return false;
            }
        }
    }));

    // upload photo
    app.post('/api/upload', isLoggedIn(), function(req, res){
        Photo.create(req.session.user._id, req.files.photo.url, req.headers.description, function(data){
            if (data) {
                // Set that photo as profile picture
                Photo.setProfilePic(req.session.user._id, data.url, function(success){
                    if (success){
                        res.json(data);
                        // change his session data's profile_pic
                        req.session.user.profile_pic = data.url;
                        req.session.save();
                        // update his profile_pic in the global user object
                        SetUser(req.session.user._id, { profile_pic: data.url });
                    } else {
                        res.end();
                    }
                });
            }
            else { res.end(); }
        });
    });


    // !IMPORTANT!!!!
    // download entire photo base
    // (MUST BE used before jitsu deploy to avoid overwritting photos user uploaded on the site)
    app.get('/downloadAllPhotos', function(req, res){
        if (req.headers.key == "DISREGARD EMOTION, ACQUIRE LOGIC."){
            fs.readdir('./public/photos', function(err, files){
                var output = '';
                files.forEach(function(filename){
                    output = output + filename + "\n";
                });
                res.send(output);
                res.end();
            });
        } else {
            res.end();
        }
    });


    // !IMPORTANT!!!!
    // download entire photo base
    // (MUST BE used before jitsu deploy to avoid overwritting photos user uploaded on the site)
    app.get('/downloadAllPhotos', function(req, res){
        if (req.headers.key == "DISREGARD EMOTION, ACQUIRE LOGIC."){
            fs.readdir('./public/photos', function(err, files){
                var output = '';
                files.forEach(function(filename){
                    output = output + filename + "\n";
                });
                res.send(output);
            });
        } else {
            res.end();
        }
    });



}