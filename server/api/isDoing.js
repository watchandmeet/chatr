// display what the user is doing
// everytime user goes to a page, it updates his 'isDoing' field in global members/guests variable


// dependencies
var User = require('../../models/user.js');
var _ = require("underscore");


// ----------------- helper functions ----------------- //

// capitalize first letter
function cap(word){
    return word.charAt(0).toUpperCase() + word.substring(1);
}



var is = {};

// setter (modifying GLOBAL members/guest's "isDoing" field)
function set(username, action, link){
    link = link ? '/' + link.toString().replace(',','/') : null;
    if (members[username]){
        members[username].isDoing = { action: action, link: link };
    } else if (guests[username]){
        guests[username].isDoing = { action: action, link: link };
    }
}

// getter (by checking GLOBAL members/guest's "isDoing" field)
is.getCurrentAction = function(username){
    var output;
    if (members[username]){
        output = members[username].isDoing;
    } else if (guests[username]){
        output = guests[username].isDoing;
    } else {
        output = { action: 'is offline', link: null };
    }
    return (output || { action: 'is just chilling around', link: null } );
}


// in random page
is.chilling = function(username){
    set(username, "is just chilling around");
};

// in room, waiting for players to join (see routes.js)
is.waiting = function(username, game, link){
    set(username, "is waiting to play " + cap(game), link);
};

// during gameplay (see drawctionary/main.js)
is.playing = function(username, game){
    set(username, "is playing " + cap(game));
};

// offline (see routes.js)
is.off = function(username){
    set(username, "is offline");
};

// initiate socket binding
is.start = function(){
    sessionSockets.of('/notification').on('connection', function (err, nocket, session) {
        if (!session || !session.user) return;

        // disconnects
        nocket.on('disconnect', function(){
            setTimeout(function(){
                if (!isOnline(session.user.username)) {
                    is.off(session.user.username);
                }
            }, 5000);
        });

        // playing game (called inside room.js when game actually starts)
        nocket.on('is_playing', function(game_name) {
            is.playing(session.user.username, game_name);
        });

        // 'isDoing' is called immediately when users goes on any page, it analyzes the url and gives a result
        // paths is an array containing each segment of url seperated by '/' (see standard.js)
        nocket.on('isDoing', function(paths){

            // waiting in a game room (check if paths has game name and room id)
            if (paths[0] && _.contains(ALLGAMES, paths[0].toLowerCase()) && paths.length > 1){
                is.waiting(session.user.username, paths[0], paths);
            }

            // anywhere else
            else {
                is.chilling(session.user.username);
            }
        });

    });

    return is;
};



module.exports = is;

