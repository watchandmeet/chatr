// not an actual model
// it fetch user's profile information

// load the things we need
var _ = require('underscore');
var async = require('async');
var User = require('../models/user.js');


// contains module.exports functions
var Profile = {};

// fields that on about me page
var fields = Profile.fields = ['username', 'profile_pic', 'name', 'status', 'school', 'major', 'interest', 'about_me'];


// fetch a profile
Profile.fetch = function(userId, callback){
    User.model.findOne({ _id: userId }, function (err, user) {
        if (err) { console.error(err); callback(false); }
        else if (user){  // if found user
            callback(_.pick(user, fields));
        }
        else { // if not found
            console.log('fetch Profile ' + userId + ' does not exist');
            callback(null);
        }
    });
};



// view a profile
Profile.view = function(username){
    User.model.update (
        { username: username },
        { $inc: { view: 1 } },
        function (err, success) {
            if (err) { console.log ('view someone err: ' + err); }
        }
    );
};


// check if each value in object is valid and (not being used atm: respects the max 38 characters rule )
Profile.checkFields = function(modificationObj){
    for (var field in modificationObj) {
        if ((!_.contains(Profile.fields, field)) /* || (field != 'about_me' && obj[field].length > 38) */ ){
            return false;
        }
    }
    return true;
}

// edit user profile, modification can be multiple (ex: contains edit for name, school, major at the same time)
Profile.edit = function (id, modification, callback) {
    if (!Profile.checkFields){
        callback(false); return;
    }
    User.model.update (
        { _id: id },
        { $set : modification },
        function (err, success) {
            if (err) { console.log ('Profile edit err: ' + err); callback (false); }
            else if (success) { callback (true); }
            else { callback(false); }
        }
    );
};




module.exports = Profile;