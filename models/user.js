// !TODO
// Friend request fetch EVERY friend request, with no limit, so eats a lot of resources.
// So, should add limit in the future.

// load the things we need
var mongoose = require('mongoose');	//Mongoose data base library
var _ = require('underscore');
var async = require('async');

// helper functions
var remove = function(item, arr, callback){
	var success = false;
	for(var i = arr.length - 1; i >= 0; i--) {
	    if(arr[i] == item) {
	       arr.splice(i, 1);
	       success = true;
	    }
	}
	callback(success);
};

// join unread and read arrays, returns new array
var all = function(obj){
	return obj.unread.concat(obj.read);
};

// Move an id to the last position in the array
var moveIdToLast = function (arr, element, callback) {
    remove(element, arr, function(success){
    	if (success){
    		arr.push(mongoose.Types.ObjectId(element));
    		callback(true);
    	} else { callback(false); }
    });
};

// check if an object is empty, aka {}
var isEmpty = function(obj) {
    return Object.keys(obj).length === 0;
}

// adds an element to the array if it does not already exist
var pushIfNotExist = function(arr, element) {
    if (!_.contains(arr, element)){
    	arr.push(element);
    }
};

// output lower cased. used as setter in username and email to avoid duplicates
function toLower(v) {
  return v.toLowerCase();
}

var databaseTable = {
	username: {
		type: String,
		required: true,
		index: { unique: true },
		set: toLower
	},
	isDoing: {
		type: String
	},
	password: {
		type: String
	},
	profile_pic: {
		type: String,
		default: '/images/default_profile_pic.png'
	},
	friend_list: {
		type: Array,
		default: []
	},
	friend_request: {
		unread: [], read: []
	},
	message: {
		type: Array,
		default: []
	},
	notification: {
		type: Array,
		default: []
	},
	notification_unseen:{
		type: Number,
		default: 0
	},
	status: {
		type: String,
		default: ''
	},
	photo:{
		type: Array,
		default: []
	},
	name: {
		type: String,
		default: ''
	},
	major: {
		type: String,
		default: ''
	},
	school: {
		type: String,
		default: ''
	},
	interest: {
		type: String,
		default: ''
	},
	about_me: {
		type: String,
		default: ''
	},
	view: {
		type: Number,
		default: 0
	}

};

// define the schema for our user model
var userSchema = mongoose.Schema(databaseTable);


// contains module.exports functions
var User = {};

// create the model for users
var model = User.model = mongoose.model('user', userSchema);

var allFields = User.allFields = _.keys(databaseTable);
allFields.push('_id');

var publicFields = User.publicFields = _.without(allFields, 'password', 'message');
var privateFields = User.privateFields = _.without(allFields, 'password');




User.fetchCostum = function(userId, costumFields, callback){
	if (userId && _.size(costumFields) > 0){
		model.findOne({ _id: userId }, function (err, user) {
			if (_.isFunction(callback)){
				var result;
				if (err) { console.error('fetch costum error: ' + err); return; }
			 	else if (user){	// if found user
			 		result = _.pick(user, costumFields);
			 	}
			 	else { // if not found
			 		console.log('fetchCostum: User ' + username + ' does not exist');
			 		result = null;
			 	}
			 	callback(result);
			}
		});
	} else {
		console.log('fetchCostum no userId or costumField is empty. Userid: ' + userId + ', costumFields: ' + costumFields);
		if (_.isFunction(callback)) callback(false);
	}
};

// same as fetchCostum, but for many users
// it returns a JSON object that matches user id with his info
User.fetchCostum_many = function(userId_arr, costumFields, callback){
	var data = {};
	async.each(userId_arr, function (userId, _callback) { // for each guy in username array
	    User.fetchCostum(userId, costumFields, function(result){ // get his public info => {username: john, level: 32}
			if (result){
				data[userId] = result; // put his friend result into final data object
				_callback();
			}
			else { callback(false); return; }
		});
	}, function(err){
		if (!err) { callback(data); } // result will json
	});
};


User.signup = function(username, password, callback){
	var user = new model({ username: username, password: password });
	user.save(function(err, userData){
		if(err) {
			console.log(err);
			callback(false);
		} else if (userData) {
			callback(_.pick(userData, privateFields));
		}
		else {
			callback(false);
		}
	});
};


User.login = function(username, password, callback){
	model.findOne({
		username: username, // if it matches username or email
		password: password // and it matches password
	}, function (err, userData) {
		if (err) {
			console.error(err);
			callback(false);
		}
	 	// if found user
	 	if (userData){
	 		callback(_.pick(userData, privateFields));
	 	}
	 	// if not found
	 	else {
	 		callback(false)
	 	}
	})
};

User.me = function(userId, callback){
	model.findOne({
		_id: userId // if it matches username or email
	}, function (err, userData) {
		if (err) {
			console.error(err);
			callback(false);
		}
	 	// if found user
	 	if (userData){
	 		callback(_.pick(userData, privateFields));
	 	}
	 	// if not found
	 	else {
	 		callback(false)
	 	}
	})
};


// add a messgae id to user's message field
User.addMessage = function (participants_arr, convoId, callback) {
	model.update (
		{ _id: { $in: participants_arr } }, // find a message id that matches (this $in selector might not work, need test)
		{ $push : { message : convoId } }, // add message id to message array
		{ multi: true },
		function (err) {
			if (err) { console.log ('adding message to User doc err: ' + err); callback (false); }
			else { callback (true); }
	});
};




module.exports = User;


