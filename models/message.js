// load the things we need
var mongoose 	= require ('mongoose');
var _ 			= require ('underscore');
var async 		= require ('async');
var User = require('../models/user.js');


// ============================= Logic explained =============================

// When someone is on someone's profile, and clicks on 'chat', the server will first try to fetch their
// conversation from message database by querying the 'participants' field, which is a string
// version of an array that contains each of their ids in a sorted way. If database can't find their
// conversation, it assumes it doesn't exist and will try to 1) create their conversation in the message
// database, and 2) append the conversation ID to each of their user database.




// helper functions
var arrExtend = function(arr){
	var res = [];
	arr.forEach(function(pair){
		res.push(_.extend(pair[0], pair[1]));
	});
    return res;
};

// Convert comma separated string to array
function strToArr(str){
	return str.split(',');
};
function arrToStr(arr){
	return arr.sort().join(",");
};

// remove element from arr
var remove = function(item, arr, callback){
	var success = false;
	for(var i = arr.length - 1; i >= 0; i--) {
	    if(arr[i] == item) {
	       arr.splice(i, 1);
	       success = true;
	    }
	}
	callback(success);
};

// Move an id to the last position in the array
function moveIdToLast(arr, id, callback) {
	if (_.last(arr) == id) {
		callback(false); return;
	}
    remove(id, arr, function(success){
    	if (success){
    		arr.push(mongoose.Types.ObjectId(id));
    		callback(true);
    	} else { callback(false); }
    });
}

// define the schema for our message model
var databaseTable = {

	participants: { // comma separated string containing participants' names
		type: String,
		index: { unique: true }
	},

	participants_arr: { // array of participants
		type: Array,
		default: []
	},

	message: {
		type: Array,
		default: []
	},

	last_updated : {
		type: Date,
		default: Date.now
	},

	seenBy: {
		type: Array,
		default: []
	}
};

var messageSchema = mongoose.Schema (databaseTable);


// contains module.exports functions
var Message = {};

// create the model for users
var model = Message.model = mongoose.model('message', messageSchema);

// user field is used to store user info in some functions, thats why we put it here so we need to concat everytime
var messageField = _.keys(databaseTable).concat(['_id', 'user']);

// fetch user's all conversations
Message.fetchAll = function (userId, callback) {

	User.fetchCostum(userId, ['message', 'username', 'profile_pic'], function (myObj) { // ids of message documents (contained in messages collection)
		var ids = myObj.message; // do data.message.slice(-6) to select last 6 messages

		if (ids.length == 0) { // if the guy has no message..
			callback ([]); return;
		}
		else { // (STEP 1) if he has message, get all his convo with '$in' selector
			model.find ({ _id: { $in: ids } },
			'_id participants participants_arr message seenBy last_updated', { sort: { last_updated: -1 } },
			function (err, allConvo) {
				if (err) { console.log ('Fetching all message err: ' + err); callback (false); return; }

				else if (allConvo) { // (STEP 2) get each conversation participant's profile picture + username (except mine, cuz its already queried above)
					async.each(allConvo, function (convo, _callback) {

						// 'message' field: array that only contains the last message of the conversation
						convo.message = [_.last(convo.message)];

						// 'others' field: array of people user is talking to in the conversation
						convo.others = _.without(convo.participants_arr, userId);

						// 'user' field: hashtable that stores all participants' information in the conversation
						convo.user = {};
						convo.user[userId] = { username: myObj.username, profile_pic: myObj.profile_pic };

						// it first stores user's own info in it
						if (_.isEmpty(convo.others)) {
							convo.others = [userId];
							_callback();
						}

						// then it queries other participants' info
						else {
						    User.fetchCostum_many(convo.others, ['username', 'profile_pic'], function(userObj){
								if (userObj){
									_.extend(convo.user, userObj);
								}
								_callback();
							});
						}
					}, function(err){
						if (!err) {
							var result = [];
							// for each conversation user has
						    _.each (allConvo, function (element) {
						    	// pick the useful fields
						        var obj = _.pick(element, '_id', 'participants', 'participants_arr', 'message', 'seenBy', 'last_updated', 'profile_pic', 'others', 'user');
						        result.push(obj);
						    });
							callback(result);
						}
					});
				}
				else { callback (false); }
			});
		}
	});
};


// fetch one conversation providing the participants of this conversation
Message.fetchByParticipants = function (participants, userId, callback) {

	if (!participants) { callback (false); return; }

	else { // start to search
		model.findOne ({ participants: participants }, function(err, convo){
			if (err) { console.log ('fetchByParticipants err: ' + err); callback(false); return; }

			else if (convo) { // found conversation, fetch all participants' picture
				User.fetchCostum_many(convo.participants_arr, ['username', 'profile_pic'], function(userObj){
					if (userObj){
						convo.user = {};
						_.extend(convo.user, userObj);
					}
					callback(_.pick( convo, messageField ) );
				});
			}

			else { // if didn't find conversation, create it and return conversation id only
				var message = new model ({
					participants: participants,
					participants_arr: strToArr(participants),
					message : [],
					seenBy : [userId]
				});

				message.save (function (err, convo) {
					if (err) { console.log ('fetchByParticipants creating message err: ' + err); callback (false); return; }
					else if (convo) {	// if succeed, link that message id to each participant's user document
						User.addMessage (convo.participants_arr, convo._id, function (success) {
							if (success) {
								User.fetchCostum_many(convo.participants_arr, ['username', 'profile_pic'], function(userObj){
									if (userObj){
										convo.user = {};
										_.extend(convo.user, userObj);
									}
									callback(_.pick( convo, messageField ) );
								});
							}
							else { callback(false); }
						});
					}
				});
			}

		});
	}
};



// fetch one conversation providing the convoId
Message.fetchById = function (convoId, userId, callback) {
	model.findOne ({ _id: convoId, participants_arr: { $in: [userId] } }, function(err, convo){
		if (err) { console.log ('Message.fetchById err: ' + err); callback(false); return; }
		else if (convo) { // found conversation
			User.fetchCostum_many(convo.participants_arr, ['username', 'profile_pic'], function(userObj){
				if (userObj){
					convo.user = {};
					_.extend(convo.user, userObj);
				}
				callback(_.pick(convo, messageField));
			});
		}
		else { // didn't find conversation
			callback(false);
		}
	});
};



// insert a message in a conversation
Message.update = function (userId, convoId, content, callback) {

	var date = new Date ().toISOString ();
	model.findOneAndUpdate (
		// find the conversation that matches, participants field is to make sure all participants are actually in the convo
		{ _id: convoId, participants_arr: { $in: [userId] } },
		{ $push : {   // add message to message array
			message : {
				sender : userId,
				content : content,
				date: date
			}
		}, $set: {
			seenBy : [userId],
			last_updated: date
		}},
		function (err, data) {
			if (err) { console.log ('updating message err: ' + err); callback (false); return; }
			else if (data) { callback (data); } // when succeed, return the updated data
			else { callback(false); }
	});
};


// update conversation order for all participants in each of their user database respectively
Message.updateConvoOrder = function(participants_arr, latestConvoId){
	_.uniq(participants_arr).forEach(function(userId){

		// for each participant of conversation
		User.model.findOne({ _id: userId }, function(err, user){
			if (err) { console.log('User.updateConvoOrder err: ' + err + ' for: ' + userId); return; }
			else if (user){

				// move this conversation id to last, only if needed
				moveIdToLast(user.message, latestConvoId, function(neededToMove){
					if (neededToMove){
						user.save(function (err, userData) {
					        if(err) {
					        	console.log('User.updateConvoOrder moveIdToLast error: ' + err + ' for: ' + userId);
					        	return;
					        }
					    });
					}
				});
			}
		});



	});
};



module.exports = Message;



