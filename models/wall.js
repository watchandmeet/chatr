// load the things we need
var mongoose    = require ('mongoose');
var _           = require ('underscore');
var async       = require ('async');
var User = require('../models/user.js');

//-------------------> helper functions <----------------------//

// remove element from array
var remove = function(item, arr, callback){
    var success = false;
    for(var i = arr.length - 1; i >= 0; i--) {
        if (arr[i] == item) {
           arr.splice(i, 1);
           success = true;
        }
    }
    callback(success);
};

// define the schema for our wall model
var wallSchema = mongoose.Schema ({

    username: { // username is the person who writes the post, aka author
        type: String
    },

    time_created : {
        type: Date,
        default: Date.now
    },

    content: {
        type: String,
        default: ''
    },

    like: {
        type: Array,
        default: []
    },

    comment: {
        type: Array,
        default: []
    }

});


// contains module.exports functions
var Wall = {};

// create the model for wall
var model = Wall.model = mongoose.model('wall', wallSchema);


// fetch user's wall's post given an array of ids
Wall.fetchByIds = function (ids, callback) {
    if (ids.length <= 0) { // if no ids is given, return empty array
        callback ({ post: [], participants: {} });
    } else { // (STEP 1) if the guy has wall posts, start to search [# query = # posts]
        model.find ({ _id: { $in: ids } },
        '_id username time_created content like comment', { sort: { time_created: -1 } },
        function (err, data) {
            if (err) { console.log ('Fetching wall err: ' + err); callback (false); }

            if (data) {
                // (STEP 2) get profile pictures for each post author + each person who comments on that post
                // [# of query = # of different commenter and post author ]
                var posts_authors = _.pluck(data, 'username');
                var posts_commenters = [];
                data.forEach(function(post){
                    if (post.comment.length > 0){
                        posts_commenters = _.union(posts_commenters, _.pluck(post.comment, 'username'));
                    }
                });
                User.fetchMany_public2(_.union(posts_authors, posts_commenters), function(participants){
                    if (participants){
                        callback ({ post: data, participants: participants });
                    } else { callback (false); }
                });

            } else { callback (false); }
        });
    }
};


// create a post on user's wall, if user is writing on his own wall, set it as his new status
// author: person who writes the post, target: the person whose wall author writes on
Wall.createPost = function(target, author, author_pic, content, callback){
    callback = _.isFunction(callback) ? callback : function(){};
    // (STEP 1) create a new document in 'Walls' database
    var post = new model ({
        username: author,
        content: content
    });
    post.save (function (err, data) {
        if (err) { console.log ('creating wall post err: ' + err); callback (false); }
        if (data) {
            var operation = {};
            operation.$push = { wall : data._id };
            // if user is writing on his own wall, set it as his new 'status'
            if (target == author){
                operation.$set = { status : content };
                if (members[target]){
                    members[target].status = content; // updates his 'status' on global 'members' variable
                }
            }
            // (STEP 2) insert post ID in target's database [1 query]
            User.model.update (
                { username: target },
                operation,
                function (err, rowAffected) {
                    if (err) { console.log ('insert post ID to User doc err: ' + err); callback (false); }
                    else if (rowAffected){
                        var result = _.pick(data, ['_id', 'username', 'time_created', 'content', 'like', 'comment']);
                        result.profile_pic = author_pic;
                        callback(result);
                    }
                    else { callback (false); }
                }
            );
        }
    });
};


// create a comment on a post
Wall.createComment = function(id, commenter, content, callback){
    // (STEP 1) add comment in wall's database [1 query]
    var comment = {
        username : commenter,
        time_created : new Date().toISOString(),
        content : content,
        like: []
    };
    model.findOneAndUpdate(
        { _id: id },
        { $push : { comment : comment }}, // add comment
        { multi: false },
        function (err, result) {
            if (err) { console.log ('create a comment on a post err: ' + err); callback (false); }
            if (result) {

                // (STEP 2) get profile picture of the commenter [1 query]
                User.fetchCostum(commenter, ['profile_pic'], function(pic){
                    if (pic){
                        comment.profile_pic = pic.profile_pic;
                        callback({ comment: comment, post: result });
                    } else { callback(false); }
                });

            } else { callback(false); }
        }
    );
};


// like / unlike a post
Wall.likePost = function (username, id, action, callback) {
    var operation = (action == 'like') ? { $addToSet: { like: username } } : { $pull: { like: username } };
    model.findOneAndUpdate(
        { _id: id },
        operation,
        { multi: false },
        function(err, data){
            if (err) { callback(false); console.log('wall like post err ' + err); }
            if (data) { callback(data); }
            else { callback(false); }
        }
    );
};


// like/unlike a comment
// id: id of post, index: position of comment in the comment array, action: either 'like' or 'unlike'
Wall.likeComment = function (username, id, index, action, callback) {
    var update = {};
    update['comment.' + index + '.like'] = username;
    var operation = (action == 'like') ? { $addToSet: update } : { $pull: update };
    model.findOneAndUpdate(
        { _id: id },
        operation,
        { multi: false },
        function(err, data){
            if (err) { callback(false); console.log('wall like comment err ' + err); }
            if (data) { callback(data); }
            else { callback(false); }
        }
    );
};




module.exports = Wall;



