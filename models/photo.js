// load the things we need
var mongoose    = require ('mongoose');
var _           = require ('underscore');
var async       = require ('async');
var User        = require('../models/user.js');
var fs          = require('fs');


// define the schema for our photo model
var photoSchema = mongoose.Schema ({

    userId: { // username is the who photo belongs to
        type: String
    },

    url: {
        type: String
    },

    time_created : {
        type: Date,
        default: Date.now
    },

    content: {
        type: String,
        default: ''
    },

    like: {
        type: Array,
        default: []
    },

    comment: {
        type: Array,
        default: []
    }

});


// contains module.exports functions
var Photo = {};

// create the model
var model = Photo.model = mongoose.model('photo', photoSchema);



// create in database a newly uploaded picture
Photo.create = function(userId, url, content, callback){
    // (STEP 1) create a new document in 'Photo' database
    var photo = new model ({
        userId: userId,
        url: url,
        content: content
    });
    photo.save (function (err, data) {
        if (err) { console.log ('creating photo err: ' + err); callback (false); }
        if (data) {
            // (STEP 2) insert photo ID in target's database [1 query]
            User.model.update(
                { _id: userId },
                { $push : { photo : data._id } }, // add message id to message array
                { multi: false },
                function(err, success){
                    if (err) { callback(false); console.log('add photo id to user err ' + err); }
                    if (success) { callback(data); }
                    else { callback(false); }
                }
            );
        } else { callback(false); }
    });
};

// set a photo as profile picture
Photo.setProfilePic = function(userId, url, callback){
    // (STEP 1): delete the pointer (photo id) in user's document
    User.model.update(
        { _id: userId }, // we put username to make sure the photo with this id really belongs to this username
        { $set: { profile_pic: url } },
        { multi: false },
        function(err, success){
            if (err) { callback(false); console.log('setProfilePic err ' + err); }
            if (success) {
                callback(true);
            } else { callback(false); }
        }
    );
}



module.exports = Photo;


