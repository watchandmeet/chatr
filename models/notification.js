// load the things we need
var mongoose    = require ('mongoose');
var _           = require ('underscore');
var async       = require ('async');
var User = require('../models/user.js');

//-------------------> helper functions <----------------------//



// define the schema for our notification model
var notificationSchema = mongoose.Schema ({

    username: { // username is the person whose notification belongs to
        type: String
    },

    content: {
        type: String,
        default: ''
    },

    link: {
        type: String,
        default: ''
    },

    from: {
        type: String,
        default: ''
    },

    postId: {
        type: String,
        default: ''
    },

    type: {
        type: String,
        default: ''
    },

    time_created : {
        type: Date,
        default: Date.now
    },

    read : {
        type: Boolean,
        default: false
    }


});


// contains module.exports functions
var Notification = {};

// create the model for notification
var model = Notification.model = mongoose.model('notification', notificationSchema);


// new notification
// postId: if it's related to a post, it's the _id of that post document
// commentIndex: if it's related to a comment, it's the index of that comment in that post
Notification.create = function(from, to, content, link, postId, type, callback){
    var modelObj = {
        username: to,
        content: content,
        link: link,
        from: from,
        postId: postId,
        type: type
    };

    var notif = new model(modelObj);

    // (STEP 1) create notification document
    notif.save (function (err, data) {
        if (err) { console.log ('creating notification err: ' + err); return; }
        if (data) {
            // (STEP 2) insert notification ID in target's database and increase notification_unseen by 1
            // [1 query]
            User.model.update (
                { username: to },
                { $push : { notification : data._id }, // add notification id
                  $inc: { notification_unseen: 1 } // increment the red notification bubble by 1
                },
                function (err) {
                    if (err) { console.log ('insert notification ID to User doc err: ' + err); callback(false); return; }
                    else { callback(data._id); }
                }
            );
        }
    });
};


// fetch notifications
var notificationFields_str = '_id username content link from postId type time_created read';
var notificationFields_arr = _.union(notificationFields_str.split(" "), ['pic']);
Notification.fetchByIds = function(ids, username, callback){
    if (ids.length <= 0) { // if no notification is given..
        callback ([]);
    } else {
        // (STEP 1): search all documents
        model.find ({ _id: { $in: ids }, username: username },
        notificationFields_str, { sort: { time_created: -1 } },
        function(err, data){
            if (err){ console.log('Notification.fetchByIds err: ' + err); callback(false); return; }
            else if (data){
                // (STEP 2): get all pictures that represent each notification
                // [# of query = # of different people's pics ]
                var people = _.uniq(_.pluck(data, 'from')); // get all unique, different people
                User.fetchProfilePics(people, function(pics){ // query their profile picture
                    if (pics){
                        var result = [];
                        _.each(data, function(notif){ // for each notification, set a picture for it
                            notif.pic = pics[notif.from];
                            result.push(_.pick(notif, notificationFields_arr));
                        });
                        callback(result);
                    } else { callback(false); return; }
                });
            } else { callback(false); return; }
        });

    }
};


// seen notification
Notification.seen = function(username, callback){
    User.model.update (
        { username: username },
        { $set : { notification_unseen : 0 } }, // set notification_unseen to 0
        function (err) {
            if (err) { console.log ('Notification.seen err: ' + err); return; }
            else { callback(true); }
        }
    );
};

// read notification, input: ids is an array, doesn't provide callback
Notification.read = function(username, ids, callback){
    model.update (
        { _id: { $in: ids }, username: username },
        { $set : { read : true } },// add message id to message array
        { multi: true },
        function (err) {
            if (err) { console.log ('Notification.read err: ' + err); callback (false); }
            else { callback (true); }
    });
};


module.exports = Notification;



