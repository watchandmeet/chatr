// set up ======================================================================
// get all the tools we need
var http = require('http')
  , path = require('path')
  , connect = require('connect')
  , express = require('express')
  , bodyParser = require('body-parser')
  , session = require('express-session')
  , app = express();
var cookieParser = require('cookie-parser')('IM NOT WEARING ANY PANTS')
  , sessionStore = new connect.middleware.session.MemoryStore();
var server = http.createServer(app);
GLOBAL.io = require('socket.io').listen(server,  { 'destroy buffer size': Infinity }); // io is global variable
var SessionSockets = require('session.socket.io'); // allowing socket to have session -> security
GLOBAL.sessionSockets = new SessionSockets(io, sessionStore, cookieParser); // session socket is global
var mongoose = require ('mongoose');
var Schema = mongoose.Schema;
var _ = require('underscore');
server.listen(Number(process.env.PORT || 3000));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json()); // get information from html forms
app.use(cookieParser); // read cookies (needed for auth)
app.use(session({ store: sessionStore })); // session secret

// use EJS templating engine
app.engine('.html', require('ejs').__express); // Using the .html instead of .ejs
app.set('views', __dirname + '/views'); // Set the folder where the pages are kept
app.set('view engine', 'html'); // This avoids having to provide the extension to res.render()
app.use (express.static ('./public')); // statically fetching public files
app.disable('x-powered-by'); // remove x-powered-by: express header


// database
var MONGOHQ_URL = 'mongodb://jack:northkorea1@ds051170.mongolab.com:51170/chatr';
mongoose.connect(MONGOHQ_URL);



io.set('log level', 1);

require('./server/chat.js')(app); // chat
require('./server/rest.js')(app); // RESTful API
require('./server/api/upload.js')(app); // Photo upload





