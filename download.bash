# Bash script to download all photos from website to local machine,
# in current directory's "photos" folder (MUST ONLY BE EXECUTED LOCALLY)

#!/bin/bash

URL="www.talkr.jit.su"

# get list of photos on the site, and write all file names to photolist.txt
wget --header="key: DISREGARD EMOTION, ACQUIRE LOGIC." "http://$URL/downloadAllPhotos" -O photolist.txt


# if the list of photos has been downloaded successfully
if [ -s "photolist.txt" ]; then

    # download all photos to photos2
    rm -rf -- public/photos2
    mkdir public/photos2
    while read file; do
        wget "http://$URL/photos/$file" -P public/photos2
    done < photolist.txt

    # if photos2 is not empty, rename it to photos (hence overwrite the existing photos folder)
    if [ "$(ls -A public/photos2)" ]; then
        rm -rf -- public/photos
        mv public/photos2 public/photos
        count=$(ls -l public/photos/* | wc -l | xargs)
        echo "---- Successfully downloaded $count photos ----"
    fi

fi