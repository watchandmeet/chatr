$( document ).ready(function () {

    // ----------------------------- General ----------------------------- //

    // attemp to connect socket and bind all the events
    initiateSocket();

    // if logged in, renew GUI with the latest user data (this is after initiateSocket because some of the inner
    // functions require socket)
    initializeGUI();

    // record all pages (using stack-like logic)
    function PageHistory() {
        var stack = { profile: ['profile'], room: ['room'], message: ['message'] }; // the tab names are also page names
        var tab = this.tab = function(){
            return $('.tab_option[data-type=active]').attr('data-page');
        }
        this.setCurrent = function(page) {
            if (!page) return;
            stack[tab()].push(page);
            stack[tab()] = _.uniq(stack[tab()]);
        };
        this.getCurrent = function(){
            return _.last(stack[tab()]);
        };
        this.getPrevious = function(){
            var pages = stack[tab()];
            return pages[pages.length-2];
        };
        this.goBack = function(){
            if (stack[tab()].length > 1) stack[tab()].pop();
        };
        this.print = function(){
            console.log(stack);
        };
        this.room = function(){
            console.log(stack.room);
        };
    };

    // initiate page history set current page
    // set current page (default is what is been set in html, the element with data-type=active)
    window.myPage = new PageHistory();


    // this variable will record current chat room
    window.currentRoom = null;
    window.currentRoomObj = null;

    // buffer that stores people's latest information (a common use is to get profile_pic)
    window.User = {};
    if (myObj && myObj._id) { User[myObj._id] = { username: myObj.username, profile_pic: myObj.profile_pic } };

    // customize scroll bar (we reset it on every screen transition)
    function resetScroll(){
        $('.scrollable').getNiceScroll().remove();
        $('.nicescroll-rails').remove();
        var $tabScreen = $('#' + myPage.getCurrent() + 'Screen.tabScreen');
        var defaultScroll = {
            hidecursordelay: 0,
            cursorborder: '0px solid rgba(255, 255, 255, 0.6)',
            cursorwidth: '6px'
        };
        if ($tabScreen.length > 0){
            $tabScreen.find('.scrollable').niceScroll(_.extend({}, defaultScroll, {
                cursorcolor: 'rgba(0, 0, 0, 0.15)',
            }));
        }
        // chat scroll bar
        else if (myPage.getCurrent() == 'room_chat'){
            $('#roomChatScreen').find('.chat_log').niceScroll(_.extend({}, defaultScroll, {
                cursorcolor: 'rgba(255, 255, 255, 0.4)',
            }));
        }
        else if (myPage.getCurrent() == 'message_chat'){
            $('#singleChatScreen').find('.chat_log').niceScroll(_.extend({}, defaultScroll, {
                cursorcolor: 'rgba(255, 255, 255, 0.4)',
            }));
        }
        // profile preview
        else if (myPage.getCurrent() == 'profile_preview'){
            $('#profilePreviewContainer').niceScroll(_.extend({}, defaultScroll, {
                cursorcolor: 'rgba(255, 255, 255, 0.4)',
                railoffset: { top: 14 }
            }));
        }
    }

    // defines the transition animation when going from one screen to another
    // takes a json object with parameter: from, top, direction
    function screenTransition(obj){
        var basics = { effect: 'drop', duration: 300 };
        var direction = obj.direction ? obj.direction : null;
        var from = obj.from;
        var to = obj.to;

        var completeExecuted = false;
        function complete(){
            if (completeExecuted) return;
            resetScroll();
            completeExecuted = true;
            if (_.isFunction(obj.complete)) obj.complete();
        }

        // determine the direction of animation of 'from' and 'to' screens based on the direction
        // we want the whole animation we want to go (either backword: <- or forward: -> or null)
        var animation = {
            left: {
                from: _.extend({}, basics, { direction: 'right' }),
                to: _.extend({}, basics, { direction: 'left' })
            },
            right: {
                from : _.extend({}, basics, { direction: 'left' }),
                to: _.extend({}, basics, { direction: 'right' })
            },
            null: {
                from: {},
                to: { }
            }
        };

        // if we go from a screen that has tab, to a screen that doesn't have tab, we need to hide that tab
        // if we go from 'no tab' to 'have tab' screen, then we display that tab
        function displayTab(){
            var fromHasTab = $(from).hasClass('tabScreen');
            var toHasTab = $(to).hasClass('tabScreen');
            if (fromHasTab && !toHasTab){
                $('#tab').hide(animation[direction].from);
            } else if (!fromHasTab && toHasTab){
                $('#tab').show(animation[direction].to);
            }
        }

        // display the animation according to the animation we calculated above
        displayTab();
        $(from).hide(animation[direction].from);
        $(to).show(_.extend({ complete: complete }, animation[direction].to));
    }


    // check document title on incoming new notifications, calculated using the sum of all red bubbles
    // GLOBAL FUNCTION, ACCESSIBLE BY ANY OTHER .JS FILE
    var unread = 0;
    window.changeTitle = function () {
        if (myObj.guest) return;
        var title = document.title;
        if (unread > 0){ // if there's new unread on top of the ones still unread, update number
            if (title.substring(0,1) == '(' && !isNaN(title.substring(1,2)) && title.indexOf(') ') > 0){
                document.title = '(' + unread.toString() + ') ' + title.substring( title.indexOf(')')+2 );
            } else { // if theres just new unread, update number and wrap it with paranthesis
                document.title = '(' + unread.toString() + ') ' + title;
            }
        } else { // when user read all unread aleady, strip parenthese with number and make title clean
            if (title.substring(0,1) == '(' && !isNaN(title.substring(1,2)) && title.indexOf(') ') > 0){
                document.title = title.substring( title.indexOf(')')+2 );
            }
        }
    };
    $(window).focus(function () {
        unread = 0;
        changeTitle();
    });



    var loginMode = function(){ return $('#loginButton').is(':visible'); };
    var signupMode = function(){ return $('#joinButton').is(':visible'); };

    // --------------- login ---------------- //
    function login(){
        var username = $('#username').val();
        var password = $('#password').val();
        $.post('/login', { username: username, password: password }, function(data){
            if (data && data.success){
                loginSuccess(data);
            } else {
                $('#chatContainer').animatedShow('shake');
            }
        });
    }
    $('#loginButton').click(login);
    $('#username, #password').keypress(function(e) {
        if(e.which == 13 && loginMode()) {
            login();
        }
    });

    // --------------- sign up ------------------ //
    function signup(){
        var username = $('#username').val();
        var password = $('#password').val();
        $.post('/signup', { username: username, password: password }, function(data){
            if (data && data.success){
                loginSuccess(data);
            } else {
                $('#chatContainer').animatedShow('shake');
            }
        });
    }
    $('#joinButton').click(signup);
    $('#username, #password').keypress(function(e) {
        if(e.which == 13 && signupMode()) {
            signup();
        }
    });


    // switch between login or join mode
    $('#register').click(function(){
        $('#username').val('').focus();
        $('#password').val('');
        if (signupMode()){
            $('#joinButton').hide();
            $('#loginButton').show();
            $(this).text('Register');
        } else {
            $('#loginButton').hide();
            $('#joinButton').show();
            $(this).text('Login');
        }
    });

    function loginSuccess(data){
        _.extend(myObj, data); // renew myObj
        initiateSocket();
        initializeGUI();
        screenTransition({ from: '#loginScreen', to: '#roomScreen', direction: 'right' })
    }






    // =========================== Create GUI according to user's data =========================== //

    // Create GUI according to myObj's data
    function initializeGUI(){
        if (myObj.guest) return; // if user is not logged in, don't go further
        initializeProfile(); // set data on his profile ('Me' section)
        fetchAllRooms(); // set data on rooms ('Rooms' section)
        fetchAllMessage(); // fetch all user's private conversations
    }

    // write each option's value from myObj
    function initializeProfile(){
        $('#profileScreen').find('.option[data-field]').each(function(){
            var field = $(this).attr('data-field');
            var value = beautify.unescape(myObj[field]);
            value = beautify.trimSpaces(value);
            if (field == 'profile_pic'){
                $(this).find('#option_pic_preview').css('background-image', 'url(' + value + ')');
            }
            else {
                var editField = $(this).find('.edit_field')
                // save each option's value into its 'original' attribute
                editField.val(value).attr('data-original', value);
            }
        });
    }


    // request for list of all rooms
    function fetchAllRooms(){
        if (!window.socket) return;
        socket.emit('fetch_all_rooms');
    }

    // display room list ('Rooms' section)
    function displayRoomList(rooms){
        _.each(rooms, function(roomObj, name){
            $(roomTemplate(roomObj)).appendTo('#room_list');
        });
        resetScroll();
        adjustRoomOrder();
    }

    // fetch user's pms (personal messages) from server
    function fetchAllMessage(){
        $.get('/message', function(data){
            if (!data) return;
            displayConvoList(data);
        });
    }

    // display pm list ('Messages' section)
    function displayConvoList(convos, fromSocket){
        var convos = $.extend({}, convos);
        var html = ''
        var listTemplate =
        "<div class='pm_container' data-convoId='<%=convoId%>'>" +
            "<div class='pm_date'><%=date%></div>" +
            "<div class='pm_pic center-cropped' style='background-image: url(<%=representative%>)'></div>" +
            "<div class='pm_content'>" +
                "<div class='pm_username slidable'><%-others%></div>" +
                "<div class='pm_preview slidable'><%=content%></div>" +
            "</div>" +
        "</div>";

        function convoTemplate(convo){
            // fetched using Message.fetchById, its responde doesn't have 'others' field, which is present if
            // we did Message.fetchAll
            if (!convo.others){
                convo.others = _.without(convo.participants_arr, myObj._id);
                convo.others = _.isEmpty(convo.others) ? [myObj._id] : convo.others;
            }
            var last_message = '';
            if (_.last(convo.message)) { last_message = _.last(convo.message).content }
            return {
                convoId: convo._id,
                date: ConvertTime(convo.last_updated),
                representative: convo.user[convo.others[0]].profile_pic,
                others: _.map(convo.others, function(id){ return convo.user[id].username }).join(', '),
                content: _replaceEmoticons(_.escape(last_message), 14)
            };
        }

        // single convo: [using Message.fetchById] (when someone pm u and u dont have this convo
        // in your pm list yet, this will create a new one in your pm list)
        if (fromSocket){
            if (!convos.convoId) return;
            $.get('/message/' + convos.convoId, function(convo){
                if (!convo) return;
                html += _.template(listTemplate)(convoTemplate(convo));
                $('#pm_list').prepend(html);
            });
        }

        // multiple convo: [using Message.fetchAll] (used at GUI initialization to display all pms of user)
        else {
            _.each(convos, function(convo){
                html += _.template(listTemplate)(convoTemplate(convo));
            });
            if (html) $('#pm_list').html(html);
        }
        displayOrRemoveEmptyMessages();

    }


    // if user has no conversation yet, show up a background screen to tell him
    function displayOrRemoveEmptyMessages(){
        if ($('#pm_list').children().length == 0){
            var html =
            "<div id='pm_list_empty' class='absoluteCenter'>" +
                "<div id='pm_list_empty_pic'></div>" +
                "<div id='pm_list_empty_title'>You have no messages yet</div>" +
                "<div id='pm_list_empty_desc'>Start by chatting with someone</div>" +
            "</div>";
            $('#pm_list').append(html);
        } else {
            $('#pm_list_empty').remove()
        }
    }







    // ------------------------- profile edit screen ----------------------------- //


    // ====== Edit Photo ===== //

    // display 'Change Photo' button
    $('#profileScreen').find('.option[data-field=profile_pic]').mouseenter(function(){
        $('#option_pic').show();
    }).mouseleave(function(){
        $('#option_pic').hide();
    });

    // display popup that slides from bottom
    $('#option_pic').click(function(){
        $('#popup_upload_photo').displayPopup();
    });

    // clean up photo preview popup, this resets the DOM element to its initial clean state after every messy upload
    // that changes the GUI
    // the code that actually displays it is in the 'rescalePhotoUpload' function
    var $photoUpload_clone = $("#photoUpload").clone(); // save element initial state
    $('#browse_photo').click(function(){
        $("#photoUpload").replaceWith($photoUpload_clone.clone()); // reset element to its initial state
    });

    // open operating system's browsing photo window
    $('#browse_photo').click(function(){
        $input = $('input[type=file].browsePhotos');
        $input.click();
        $('input[type=file].browsePhotos').get(0).addEventListener('change', handleFileSelect, false); // handle file selection
    });

    // select files
    function handleFileSelect(evt) {
        var photos = evt.target.files;
        var photosCount = photos.length;
        var count = 0;
        rescalePhotoUpload(photosCount);
        for (var i = 0; i < photosCount; i++) {
            var f = photos[i];
            if (!f.type.match('image.*')) {
                continue;
            }
            var reader = new FileReader();
            reader.onload = (function(thePhoto) {
                return function(e) { // preview screen
                    // display picture + "say something about this photo"
                    var html =
                    '<div class="photoUpload_preview_single">' +
                        '<div class="photoUpload_thumb" style="background-image: url(' + e.target.result + ')" title="' + encodeURI(thePhoto.name) + '">' +
                            '<div class="photoUpload_darken"><div class="photoUpload_progressBar"><div class="photoUpload_currentProgress"></div></div></div>' +
                        '</div>' +
                        '<div class="photoUpload_descWrapper">' +
                            '<textarea class="photoUpload_desc" placeholder="Say something about this photo..."></textarea>' +
                        '</div>' +
                    '</div>';
                    $('#photoUpload_preview').append(html);
                    // append 'Save all' button when all pics is loaded
                    if (count+1 == photosCount && !$('#photoUpload_preview').find('.photoUpload_save').length){
                        $('#photoUpload_preview').append('<div class="photoUpload_saveWrapper"><div id="photoUpload_save_browse" class="photoUpload_save popButton" enabled="true" data-default="true">Save all</div></div>');
                    }
                    // add scroll if there're more than 3 pics (hence rows >= 2)
                    if (count+1 == 3){
                        $('#photoUpload_body').niceScroll({
                            cursorcolor: 'rgba(0, 0, 0, 0.4)',
                            cursorborder: '0px solid rgba(255, 255, 255, 0.6)',
                            cursorwidth: '4px',
                            railoffset: { left: -3 },
                            railpadding: { top: 10 }
                        });
                    }
                    count++;
                };
            })(f);
            reader.readAsDataURL(f);
        }
    }

    // rescale popup according to number of pic uploaded (n)
    function rescalePhotoUpload(n){
        if (n == 1){
            $('#photoUpload').width(450).height(480);
            $('#photoUpload_body').width($('#photoUpload').width()-10);
        } else if (n == 2){
            $('#photoUpload').width(2*430+30).height(480);
            $('#photoUpload_body').width($('#photoUpload').width()-10);
        } else if (n >= 3){
            $('#photoUpload').width(2*430+30).height(500);
            $('#photoUpload_body').width($('#photoUpload').width()-10)
            .height($('#photoUpload').height() - $('#photoUpload').find('.pop_title').outerHeight(true) - 10)
        }
        // finally, display photo preview popup
        if (n > 0) $('#photoUpload').displayPop();
    }

    // submit photo form (AJAX-ish) + 'say something about this photo..' fields
    $(document)
    .on('submit', '#photoUpload_browse', function(e){ // remove page reload when submit photos
        var $this = $(this);
        var $button = $('#photoUpload_save_browse');
        if ( $button.attr('enabled') == 'true' ){ // disable upload before receive response
            $button.attr('enabled', 'false');

            // upload photo one by one
            var photoDesc = $('#photoUpload_preview').find('.photoUpload_desc');
            var uploadSuccess = 0;
            for (i = 0; i < photoDesc.length; i++) {
                var file = $this.find('.browsePhotos').get(0).files[i];
                var description = photoDesc.eq(i).val() ? beautify.trimSpaces(photoDesc.eq(i).val()) : '';
                var formData = (new FormData());

                // now since we are allowing only 1 photo upload, lets put the current photo as preview
                var photoUrl = URL.createObjectURL(file);
                $('#option_pic_preview').css('background-image', 'url(' + photoUrl + ')');

                formData.append('photo', file);
                $.ajax({
                    url: '/api/upload',
                    type: 'POST',
                    beforeSend: function (request){
                        request.setRequestHeader("description", description);
                        // !TODO: in request header, new lines in not allowed, so everytime user press enter
                        // to make new lines in photo's description box, the whole ajax request can not be sent.
                        // So instead now the workaround is replacing any new lines with one white space with
                        // beautify.trimSpaces()
                    },
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    'xhr': function() {
                        var j = i; // save pointer which photo indicated
                        var xhr = $.ajaxSettings.xhr();
                        if (xhr.upload){
                            var $parent = $('#photoUpload_preview').find('.photoUpload_darken').eq(j);
                            var $container = $parent.find('.photoUpload_progressBar');
                            var $progress = $parent.find('.photoUpload_currentProgress');
                            xhr.upload.addEventListener('progress', function(e){
                                $('#photoUpload_preview').find('.photoUpload_descWrapper').eq(j).css('background-color', 'rgba(0,0,0,0.08)').find('textarea').prop('disabled', true).css('color', '#848484'); // disable textarea
                                $parent.show(); // show progress bar
                                $progress.animate({ width: (e.loaded / e.total * $container.width()) + 'px' }); // animate upload progress
                            }, false);
                            xhr.upload.addEventListener("load", function(e){
                                uploadSuccess++; // when all pictures are loaded, close popup
                                if (uploadSuccess == photoDesc.length){
                                    $parent.show();
                                    // animate upload progress
                                    $progress.animate({ width: $container.width()+'px' }, { complete: function(){
                                        // when done, simply close the pop up
                                        $('#photoUpload').find('.pop_close').click();
                                    }});
                                }
                            }, false);
                       } return xhr;
                    }
                })
                .done(function(data){
                    myObj.profile_pic = data.url;
                });
            }
        }
       return false; // prevent page refresh on form submit
    })
    .on('click', '#photoUpload_save_browse', function(){
        $('#photoUpload_browse').submit();
    });




    // ====== Edit textual info ===== //

    // display editing icon when hover over an editable option
    $('#profileScreen').find('.option').not('[data-field=profile_pic]')
    .mouseenter(
        function(){
            $(this).find('.option_edit').show();
        }
    ).mouseleave(
        function(){
            $(this).find('.option_edit').hide();
        }
    );


    // scroll down the scrollable section if buttons are not fully visible (in reality we use the parental
    // '.option' element instead and compare if its bottom is below tab)
    function scrollIfNotFullyVisible($option){
        var distanceBelowVisibility = $option.offset().top + $option.outerHeight(true) - $('#tab').offset().top;
        if (distanceBelowVisibility > 0) $option.closest('.scrollable')[0].scrollTop += distanceBelowVisibility;
    }

    function addTextareaAnimation($textarea){
        // add growth animation to textarea (we don't do it when initiate autosize but do it now because
        // animation is shown everytime element goes from hidden to shown )
        // current not used because without scrollIfNotFullyVisible, it's actually cleaner. Simply below uncomment to use again
        // document.body.offsetWidth; // force a reflow before the class gets applied (taken from author's site)
        // $textarea.addClass('textarea-transition');
    }

    // get supposed height of textarea (without leading and trailing space)
    function setRealContentHeight($original){
        if (!$original.is('textarea')) return;
        var $clone = $original.clone(true).val($.trim($original.val())).css({ opacity: 0, position: 'fixed', top: -999999 })
        .width($original.width()).appendTo('body');
        var h = $clone.height(0)[0].scrollHeight;
        $original.height(h);
        $clone.remove();
    }

    // when type in textarea, scroll down if necessary to keep the buttons below always visible
    $('textarea.edit_field').on('input', function(){
        scrollIfNotFullyVisible($(this).closest('.option'));
    });

    // display editing GUI (textarea/input and buttons)
    $('.edit_field').click(function(){
        var parent = $(this).closest('.option');
        var editField = parent.find('.edit_field');
        // if we are in the process of editing, then don't do anything (this is to avoid the next line of code, which is reset content to original)
        if (editField.data('isEditing')) return;
        editField.val($(this).attr('data-original'));
        // if textarea, add autosize to make it grow/shrink as you type
        if (editField.is('textarea')){
            editField.autosize();
        }
        parent.find('.edit_cancel').show();
        parent.find('.edit_save').show();
        // if 'cancel' and 'save' button are not fully visible, scroll down until u fully see them
        scrollIfNotFullyVisible(parent);
        editField.data('isEditing', true);
    });

    // cancel editing
    $('.edit_cancel').click(function(){
        var parent = $(this).closest('.option');
        var editField = parent.find('.edit_field');
        parent.find('.edit_save').hide();
        parent.find('.edit_cancel').hide();
        editField.val(editField.attr('data-original'));
        setRealContentHeight(editField);
        editField.data('isEditing', false).blur();
    });

    // save editing (mostly save as cancel, except save to data-original and post request)
    $('.edit_save').click(function(){
        var parent = $(this).closest('.option');
        var editField = parent.find('.edit_field');
        var original = editField.attr('data-original');
        var changed = beautify.trimSpaces(editField.val());
        editField.attr('data-original', changed); // update original
        parent.find('.edit_cancel').click();
        if (original != changed){
            var modification = {};
            modification[parent.attr('data-field')] = changed;
            $.post("/profile", modification, function(data) {
                // if save successful
                if (data && data.success){
                    _.extend(myObj, modification);
                }
            });
        }
    });

    // when press enter, save edit
    $('.edit_field').keypress(function(e) {
        var parent = $(this).closest('.option');
        var save = parent.find('.edit_save');
        if (e.which == 13) {
            save.click();
        }
    });








    // ------------------------- choose room screen ----------------------------- //

    /* -------- bottom menu management ------- */

    // when click on a tab
    $('.tab_option')
    .click(function(){
        // make all other tabs inative, and active the clicked one
        $('.tab_option').attr('data-type', 'inactive');
        $(this).attr('data-type', 'active');
        myPage.setCurrent($(this).attr('data-page'));
        $('.tabScreen').hide();
        // show the active page
        $('#' + myPage.getCurrent() + 'Screen.tabScreen').show();
        // reset the scroll on the scrollable section of that page
        resetScroll();
    });


    // click on 'Profile' tab, make all textareas on that page fit its content
    // if we dont do this, textareas will take their default height specified by browser
    $('#tab_profile').click(function(){
        $('#profileScreen .option[data-field] textarea.edit_field').each(function(){
            setRealContentHeight($(this));
        });
    });


    // search box
    $('#roomScreen').find('.search_box').keyup(function() {
        var typed = $(this).val().toLowerCase(); // case-insensitive search
        $('#room_list').find('.room_name').each(function(){ // specify the parent id for search performance
            var name = $(this).text().toLowerCase();
            if (name.indexOf(typed) == -1){
                $(this).closest('.room_container').hide();
            } else {
                $(this).closest('.room_container').show();
            }
        });
    });

    // join a room
    $('#room_list').on('click', '.room_container', function(){
        // room name (the key of hashtable) are stored as lower case on the server
        socket.emit('join', $(this).attr('data-room'));
        // erase all existing messages
        var parent = $('#roomChatScreen');
        parent.find('.chat_input').val('')
        var chat_log = parent.find('.chat_log');
        chat_log.empty();
        myPage.setCurrent('room_chat');
        screenTransition({ from: '#roomScreen', to: '#roomChatScreen', direction: 'right', complete: function(){
                scrollDiv(chat_log);
                parent.find('.chat_input').focus();
            }
        });
    });

    // fetch room data (this is triggered upon joining a room)
    function joinSuccess(roomObj){
        window.currentRoom = roomObj.name;
        window.currentRoomObj = roomObj;
        displayChat();
    }

    // display all previous messages on screen
    function displayChat(){
        if (!window.currentRoomObj) return;
        // set title
        setChatTitle();
        // set messages
        var chat_log = $('#roomChatScreen').find('.chat_log');
        _.each(window.currentRoomObj.messages, function(msgObj){
            $(messageTemplate(msgObj)).appendTo(chat_log);
        });
        // scroll chat to bottom
        scrollDiv(chat_log);
    }

    // set current room's title
    function setChatTitle(){
        var count = _.size(window.currentRoomObj.participants);
        var title = window.currentRoomObj.original_name + " - " + count + " " + (count > 1 ? "people" : "person") + " online";
        $('#roomChatScreen').find('.screen_top_text').text(title);
    }


    // ---------- create a room ----------- //

    // display middle popup
    $('#create_room_button').click(function(){
        $('#popup2_create_room').displayPopup2();
    });

    // socket request (create room)
    $('#confirm_room_creation').click(function(){
        var name_input = $('input#create_room_name');
        var desc_input = $('input#create_room_desc');
        if (name_input.attr('data-disabled') == 'true') return;
        var room_name = beautify.trimSpaces(name_input.val());
        var room_desc = beautify.trimSpaces(desc_input.val());
        if (!room_name || !room_desc) return;
        // send request
        socket.emit('create', { original_name: room_name, desc: room_desc });
        // make GUI appear as waiting
        name_input.prop("disabled", true);
        desc_input.prop("disabled", true);
        $(this).text('Creating...');
    });

    // when press 'Create', imitate clicking
    $('#popup2_create_room').find('input').keypress(function(e) {
        if (e.which == 13) {
            $('#confirm_room_creation').click();
        }
    });

    // fail creating room - room name already taken
    function createFail(roomObj){
        $('#popup2_create_room').closePopup2(function(){
            $('#popup2_room_name_taken').displayPopup2();
        });
    }

    // socket response (room creation success)
    function createSuccess(roomObj){
        // after room created, change the button to 'Done!'
        $('#confirm_room_creation').text('Done!');
        // and immediately reset popup GUI
        $('#popup2_create_room').closePopup2();
        appendRoom(roomObj);
    }

    // append a room to the list, takes input the room object given by server
    function appendRoom(roomObj){
        joinSuccess(roomObj);
        $(roomTemplate(roomObj)).appendTo('#room_list').click();
        adjustRoomOrder();
    }

    // generate the html of a room
    function roomTemplate(obj){
        var online_count = _.size(obj.participants);
        var name = obj.name;
        var original_name = obj.original_name;
        var room_desc = obj.desc;
        var profile_pic = obj.profile_pic ? obj.profile_pic : '/images/default_room_pic_pink.png';
        if (_.isUndefined(online_count) || !original_name || !name || !room_desc) return '';

        return _.template(
            "<div class='room_container' data-room='<%- name %>' data-count='<%= online_count %>'>" +
                "<div class='room_online_count'><%= online_count %> online</div>" +
                "<div class='room_pic center-cropped' style='background-image: url(<%= profile_pic %>)'></div>" +
                "<div class='room_content'>" +
                    "<div class='room_name slidable'><%- original_name %></div>" +
                    "<div class='room_desc slidable'><%- room_desc %></div>" +
                "</div>" +
            "</div>"
        )({
            online_count: online_count, original_name: original_name, room_desc: room_desc, profile_pic: profile_pic,
            name: name
        });
    }

    // re-display room list from room with largest online count to smallest
    function adjustRoomOrder(){
        var rooms = $('#room_list').find('.room_container');
        rooms.sort(function (a, b){
            var an = parseInt(a.getAttribute('data-count')),
                bn = parseInt(b.getAttribute('data-count'));
            if(an < bn) {
                return 1;
            }
            if(an > bn) {
                return -1;
            }
            return 0;
        });
        $('#room_list').append(rooms);
    }





    // ------------------------- chat screen ----------------------------- //

    // go back to previous page (room <- chat)
    $('.goBack').click(function(){

        // room <- chat
        if (myPage.getPrevious() == 'room' && myPage.getCurrent() == 'room_chat'){
            if (window.currentRoom){
                // leave room
                socket.emit('leave', window.currentRoom);
                // decrement online count by 1
                // var parent = $('#room_list').find('.room_container[data-room=' + window.currentRoom + ']');
                // var count = parseInt(parent.attr('data-count')) - 1;
                // parent.attr('data-count', count);
                // parent.find('.room_online_count').text(count + ' online');
            }
            // reset current room and page values
            window.currentRoom = window.currentRoomObj = null;
            myPage.goBack();
            screenTransition({ from: '#roomChatScreen', to: '#roomScreen', direction: 'left' });
        }

        // chat <- profile_preview
        else if (myPage.getPrevious() == 'room_chat' && myPage.getCurrent() == 'profile_preview'){
            myPage.goBack();
            screenTransition({ from: '#profilePreviewScreen', to: '#roomChatScreen', direction: 'left', complete: function(){
                // scroll chat to bottom
                scrollDiv($('#roomChatScreen').find('.chat_log'));
                $('#roomChatScreen').find('.chat_input').focus();
            }});
        }

        // profile_preview <- singleChat
        else if (myPage.getPrevious() == 'profile_preview' && myPage.getCurrent() == 'message_chat'){
            myPage.goBack();
            screenTransition({ from: '#singleChatScreen', to: '#profilePreviewScreen', direction: 'left' });
        }

        // pm list <- singleChat
        else if (myPage.getPrevious() == 'message' && myPage.getCurrent() == 'message_chat'){
            myPage.goBack();
            screenTransition({ from: '#singleChatScreen', to: '#messageScreen', direction: 'left' });
        }

        // singlechat <- profile_preview
        else if (myPage.getPrevious() == 'message_chat' && myPage.getCurrent() == 'profile_preview'){
            myPage.goBack();
            screenTransition({ from: '#profilePreviewScreen', to: '#singleChatScreen', direction: 'left', complete: function(){
                scrollDiv($('#singleChatScreen').find('.chat_log'));
                $('#singleChatScreen').find('.chat_input').focus();
            }});
        }

    });


    // go to someone's profile (chat -> profile)
    $('.chat_log').on('click', '.message_pic', function(){
        var userId = $(this).attr('data-userId');

        // if the previous page is already his profile, simply go back instead of going forward which adds extra page
        if (myPage.getPrevious() == 'profile_preview' && $('#profilePreviewScreen').attr('data-userId') == userId){
            myPage.goBack();
            screenTransition({ from: '#singleChatScreen', to: '#profilePreviewScreen', direction: 'left' });
            return;
        }
        $.get('/profile/' + userId, function(userObj){
            if (!userObj) return;
            User[userId] = userObj;
            $('#profilePreviewScreen').attr('data-userId', userId);
            $('#profilePic').css('background-image', 'url(' + userObj.profile_pic + ')');
            $('#profileUsername').text(userObj.username);
            $('#profileStatus').text(userObj.status);
            $('#profile_name').find('.P_option_value').text(userObj.name);
            $('#profile_school').find('.P_option_value').text(userObj.school);
            $('#profile_major').find('.P_option_value').text(userObj.major);
            $('#profile_interestedIn').find('.P_option_value').text(userObj.interest);
            $('#profile_aboutMe').find('.P_option_value').text(userObj.about_me);
            // if a field is not set yet, dont display them
            $('#profileDetail').find('.profileOption').find('.P_option_value').each(function(){
                if (!$(this).text()){
                    $(this).addClass('P_option_placeholder').text('Not set yet');
                } else {
                    $(this).removeClass('P_option_placeholder');
                }
            });

            var from;
            if (myPage.getCurrent() == 'room_chat'){ from = '#roomChatScreen'; }
            else if (myPage.getCurrent() == 'message_chat'){ from = '#singleChatScreen'; }
            if (!from) return;
            screenTransition({ from: from, to: '#profilePreviewScreen', direction: 'right' });
            myPage.setCurrent('profile_preview');
        });
    });


    // initiate conversation to someone through his profile
    $('#profileChat_button').click(function(){
        var hisId = $('#profilePreviewScreen').attr('data-userId');
        var parent = $('#singleChatScreen');
        var chat_log = parent.find('.chat_log');
        var input = parent.find('.chat_input').val('');
        var participants_arr = [myObj._id, hisId].sort();
        var participants = participants_arr.join(",");

        // if the previous page is already their convo, simply go back instead of going forward which adds extra page
        if (myPage.getPrevious() == 'message_chat' && parent.attr('data-participants') == participants){
            myPage.goBack();
            screenTransition({ from: '#profilePreviewScreen', to: '#singleChatScreen', direction: 'left', complete: function(){
                scrollDiv(chat_log);
                input.focus();
            }});
            return;
        }

        // fetch his previous conversation with this guy
        $.post('/message/fetch', { participants: participants_arr }, function(convo){
            if (!convo) return;
            parent.attr('data-convoId', convo._id).attr('data-participants', convo.participants);
            _.extend(User, convo.user);
            _.each(convo.message, function(msgObj){
                msgObj = processSingleChat(msgObj);
                $(messageTemplate(msgObj)).appendTo(chat_log);
            });
            scrollDiv(chat_log);
        });
        // set chat title
        parent.find('.screen_top_text').text($('#profileUsername').text());
        // erase all existing messages
        chat_log.empty();
        myPage.setCurrent('message_chat');
        screenTransition({ from: '#profilePreviewScreen', to: '#singleChatScreen', direction: 'right', complete: function(){
            scrollDiv(chat_log);
            input.focus();
        }});
    });

    // pre-process single chat screen's message due to its difference with room chat message (sender is id, no profile_pic field)
    function processSingleChat(msgObj){
        var userId = msgObj.sender;
        msgObj.userId = userId;
        msgObj.sender = User[userId].username;
        msgObj.profile_pic = User[userId].profile_pic;
        return msgObj;
    }



    // upon incoming message, determine whether or not to automatically scroll down a chat window to the bottom
    function scrollDiv($messageContainer, messageClass, sender){
        // When user scroll up in chat box, new incoming message wont bring it down.
        // Automatic scrolldown is only triggered if one of 3 condition is satisfied:
        // 1) if i'm already at the bottom of chat, 2) if message sender is me, and 3) if messages has not filled up
        // the chat yet and therefore scrollbar is still not visible (this condition is more of a bug fix)
        // To simply scroll down the chat, just dont put the parameters 'messageClass' or 'sender'
        if (!messageClass || !sender
            || $messageContainer.scrollTop() + $messageContainer.find(messageClass).last().outerHeight(true) == $messageContainer[0].scrollHeight - $messageContainer.outerHeight(true)
            || sender == myObj.username
            || $messageContainer[0].scrollHeight - $messageContainer.find(messageClass).last().outerHeight(true) <= $messageContainer.outerHeight(true) ){
            $messageContainer[0].scrollTop = $messageContainer[0].scrollHeight;
        }
    }

    // send message
    $('.chatScreen').find('.chat_send').click(function(){
        var parent = $(this).closest('.chatScreen');
        var input = parent.find('.chat_input');
        var content = input.val().trim();
        if (!content) return;
        input.val('').focus();

        // for room chat
        if (myPage.getCurrent() == 'room_chat'){
            if (!window.currentRoom) return;
            socket.emit('room_message', { content: content, room: window.currentRoom });
        }

        // for 1v1 chat with someone
        else if (myPage.getCurrent() == 'message_chat'){
            var convoId = parent.attr('data-convoId');
            if (!convoId) return;
            socket.emit('personal_message', { content: content, convoId: convoId });
        }

    });
    $('.chatScreen').find('.chat_input').keypress(function(e) {
        if(e.which == 13) {
            $(this).closest('.chatScreen').find('.chat_send').click();
        }
    });



















    // ================================ Messages screeen ================================ //

    // search
    $('#messageScreen').find('.search_box').keyup(function() {
        var typed = $(this).val().toLowerCase(); // case-insensitive search
        $('#pm_list').find('.pm_username').each(function(){ // specify the parent id for search performance
            var name = $(this).text().toLowerCase();
            if (name.indexOf(typed) == -1){
                $(this).closest('.pm_container').hide();
            } else {
                $(this).closest('.pm_container').show();
            }
        });
    });


    // chat with someone
    $('#messageScreen').on('click', '.pm_container', function(){
        var convoId = $(this).attr('data-convoId');
        var parent = $('#singleChatScreen');
        var chat_log = parent.find('.chat_log');

        $.get('/message/' + convoId, function(convo){
            if (!convo) return;
            parent.attr('data-convoId', convo._id).attr('data-participants', convo.participants);
            _.extend(User, convo.user);
            _.each(convo.message, function(msgObj){
                msgObj = processSingleChat(msgObj);
                $(messageTemplate(msgObj)).appendTo(chat_log);
            });
            scrollDiv(chat_log);
        });
        // set chat title
        parent.find('.screen_top_text').text($(this).find('.pm_username').text());
        // erase all existing messages
        var input = parent.find('.chat_input').val('');
        chat_log.empty();
        myPage.setCurrent('message_chat');
        screenTransition({ from: '#messageScreen', to: '#singleChatScreen', direction: 'right', complete: function(){
                scrollDiv(chat_log);
                input.focus();
            }
        });
    });






    // all socket.on functions go to here
    function initiateSocket(){

        // if user is not logged in, or socket already initiated, return the function
        if (myObj.guest || window.socket) return;

        // or else, initiate the socket and start binding all the events
        window.socket = io.connect();

        // fetch list of all available rooms
        socket.on('fetch_all_rooms', function(rooms){
            displayRoomList(rooms)
        });

        // fetch messages after joining a room
        socket.on('join_success', function(roomObj){
            joinSuccess(roomObj);
        });

        // join the room after creating a room
        socket.on('create_success', function(roomObj){
            createSuccess(roomObj)
        });

        // room creation fail
        socket.on('create_fail', function(roomObj){
            createFail(roomObj);
        });

        // someone joined room
        socket.on('new_joiner', function(participants){
            if (!currentRoomObj) return;
            currentRoomObj.participants = participants;
            setChatTitle();
        });

        // someone left the room
        socket.on('leaver', function(participants){
            if (!currentRoomObj) return;
            currentRoomObj.participants = participants;
            setChatTitle();
        });

        // receive message from room
        socket.on('room_message', function(msgObj){
            if (!msgObj.content) return;
            var chat_log = $('#roomChatScreen').find('.chat_log');
            $(messageTemplate(msgObj)).hide().appendTo(chat_log).fadeIn();
            scrollDiv(chat_log, '.single_message', msgObj.sender);
        });

        // receive personal message
        socket.on('personal_message', function(msgObj){
            if (!msgObj.content) return;

            // update single chat screen
            var chat_log = $('#singleChatScreen[data-convoId=' + msgObj.convoId + ']').find('.chat_log');
            if (chat_log.length > 0){
                $(messageTemplate(msgObj)).hide().appendTo(chat_log).fadeIn();
                scrollDiv(chat_log, '.single_message', msgObj.sender);
                if (!$(window).is(":focus") && msgObj.sender != myObj.username){
                    unread ++ ;
                    changeTitle();
                }
            }

            // update pm list's preview
            var pm_preview = $('#pm_list').find('.pm_container[data-convoId=' + msgObj.convoId + ']').find('.pm_preview');
            if (pm_preview.length > 0){ // if convo exists in list, update the preview sentence
                pm_preview.html(_replaceEmoticons(_.escape(msgObj.content), 14));
            }
            else { // if convo doesn't exist, create one
                displayConvoList(msgObj, true);
            }
            // put the convo first in the list(cuz its now the most recent)
            var from = $('#pm_list .pm_container[data-convoId=' + msgObj.convoId + ']').index('#pm_list .pm_container');
            var pm_list = $('#pm_list .pm_container');
            pm_list.splice(0, 0, pm_list.splice(from, 1)[0]); // move element to front
            $('#pm_list').append(pm_list)
        });

        // someone in my room changed his profile_pic
        // socket.on('change_profile_pic', function(url){
        //     console.log(url)
        // });

    }

    function messageTemplate(msgObj){

        var userId = msgObj.userId;
        var profile_pic = msgObj.profile_pic;
        var sender = msgObj.sender;
        var type = (msgObj.sender == myObj.username) ? 'me' : 'other';
        var content = _replaceEmoticons(_.escape(msgObj.content));

        return _.template(
            "<div class='single_message' data-type='<%=type%>'>" +
                "<div class='message_person'>" +
                    "<div data-userId='<%=userId%>' class='message_pic center-cropped' style='background-image:url(<%=profile_pic%>)'></div>" +
                    "<div class='message_sender slidable'><%-sender%></div>" +
                "</div>" +
                "<div class='message_bubble'>" +
                    "<div class='message_arrow'></div>" +
                    "<div class='message_content'><%=content%></div>" +
                "</div>" +
            "</div>"
        )({ type: type, sender: sender, profile_pic: profile_pic, content: content, userId: userId });
    }








    // initialize the first screen to be seen upon refresh page
    // this has to be at the bottom because we have to wait until the click handler is attached in the above code
    if (!myObj.guest){
        $('#tab').find('.tab_option[data-type=active]').click();
    }







});
