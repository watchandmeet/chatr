/* self made */
// convert time from ISO format to a more readible format to be displayed
var ConvertTime = function (date){
    var time_now = new Date();
    var time_msg = new Date(date);
    var date_msg = time_msg.getDate(); // day of the month (from 1-31)
    var day_msg = time_msg.getDay(); // day of the week (from 0-6)
    var month_msg = time_msg.getMonth();
    var year_msg = time_msg.getFullYear();
    var dayNames = ['Sunday', 'Monday', 'Tueesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var hours_msg = time_msg.getHours();
    var minutes_msg = time_msg.getMinutes();
    var ampm_msg = (hours_msg >= 12) ? "PM" : "AM";
    hours_msg = (hours_msg >= 12) ? hours_msg-12 : hours_msg;

    // same day
    if (time_now.getDate() == date_msg && time_now.getMonth() == month_msg && time_now.getFullYear() == year_msg){
        minutes_msg = minutes_msg.toString();
        minutes_msg = (minutes_msg.length == 1) ? ('0' + minutes_msg) : minutes_msg;
        return (hours_msg + ':' + minutes_msg + ' ' + ampm_msg);
    // within 6 days
    } else if ((time_now.getTime()-time_msg.getTime()) < 518400000){
        return (dayNames[day_msg]);
    // same year
    } else if (time_now.getFullYear() == year_msg) {
        return (monthNames[month_msg] + ' ' + date_msg);
    // past year
    } else {
        return (date_msg + '/' + (parseInt(month_msg) + 1).toString() + '/' + year_msg);
    }
};