/* self made */
// format a chat message / post to display on-screen

var isMessageEmpty = function (message) {
    return (!(/([^\s])/.test (message)));
};

var beautifyAll = function (message, youtube_width, youtube_height) {
    // if width and height are not specified, youtubify will not be executed
    message = beautify.trailingSpaces (message);
    message = beautify.htmlEscape (message);
    message = beautify.spaces (message);
    message = beautify.bold (message);
    message = beautify.italic (message);
    message = beautify.newlines (message);
    message = beautify.linkify (message);
    message = beautify.youtube (message, youtube_width, youtube_height);
    return message;
};

var beautify = {

    htmlEscape: function (entry) {
        return entry.replace (/&/g, '&amp;')
            .replace (/</g, '&lt;')
            .replace (/>/g, '&gt;')
            .replace (/"/g, '&quot;')
            .replace (/'/g, '&apos;');
    },

    unescape: function(message) {
        return message.replace(/&amp;/g, '&')
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&quot;/g, '"')
        .replace(/&#039;/g, "'")
        .replace(/&#39;/g, "'"); // i added this because EJS escape apostroph using 39, instead of 039. its currently used to unescape room_name in room.js
    },

    // embed youtube video if message contains a youtube link
    youtube: function (message, width, height) {
        var youtubeUrlRegExp = /youtube.com\/watch.*[?&]v=(.{11})/i;
        if (youtubeUrlRegExp.test(message) && width && height ) {
            var videoId = RegExp.$1;
            return (message + '<br/><iframe width="' + width.toString() + '"'
                    + 'height="' + height.toString() + '" src="//www.youtube.com/embed/'
                    + videoId
                    + '" frameborder="0" allowfullscreen></iframe>');
        }
        return message;
    },

    // use *bold*
    bold: function (message) {
        return message.replace (/\*((\w|-)+)\*/g, '<b>$1</b>');
    },

    // use _italic_
    italic: function (message) {
        return message.replace (/_((\w|-)+)_/g, '<i>$1</i>');
    },

    // replace consecutive spaces with no-break spaces
    spaces: function (message) {
        return message.replace (/( {2,})([^\n]|$)/g, function (spaces) {
            // replace with no-break spaces
            spaces = spaces.replace (/ /g, '&nbsp;');
            return spaces;
        });
    },

    // removes trailing spaces
    trailingSpaces: function (message) {
        return message.replace (/\s+$/, '');
    },

    // removes beginning and trailing spaces, and replace any consecutive space in the middle with one
    // hence remove ALL useless spaces
    trimSpaces: function (message) {
        return $.trim(message.replace(/\s+/g, ' '));
    },

    newlines: function (message) {
        return message.replace (/\n/g, '<br />');
    },

    // replace all <br> by \n, then unescape
    textareaLines: function(message){
        return beautify.unescape(message.replace(/<br\s*[\/]?>/gi, "\n"));
    },

    // Capitalize the first letter of string
    capitalize: function(message){
        return message.charAt(0).toUpperCase() + message.slice(1);
    },

    // +1 on a DOM element text
    add1: function($div){
        $div.text( parseInt($div.text()) + 1 );
    },

    // get random element from an array. This will pop last element from array, and when the array is empty, it
    // will shuffle and array, and start poping again. Hence generally you'll not see 2 same element twice in a row
    // how to use:
    // 1) Initiate object: var random = new beautify.Random(array);
    // 2) Whenever you call random(), it will return a random element from the array
    Random: function(arr) {
        var shuffled_array = _.shuffle(arr);
        return function(){
            if (_.isEmpty(shuffled_array)){
                shuffled_array = _.shuffle(arr);
            }
            return shuffled_array.pop();
        };
    },

    emotions: function(){
        return ['LOL!', 'Srsly.', 'ZOMFG!', 'Whoa!', 'Hot!', 'Sweet!', 'Really!',
        'OH! SNAP!', 'w00t!', 'Boioioioing.', 'Cow level pl0x.', 'SHOOP!', 'YARR!', 'SAUCY!',
        'OMGz.', 'R0FL!', 'Wat!', 'lel.', 'shizznizz!', 'Oooh, damn.'].slice(0);
    },

    // detect link in chat & posts and make them actual url
    linkify: function(message){
        var urlRegex = /(https?:\/\/[^\s]+)/g;
        return message.replace(urlRegex, function(url) {
            return '<a href="' + url + '" target="_blank" class="pseudo-link_blue">' + url + '</a>';
        })
    },

    // upon incoming message, determine whether or not to automatically scroll down a chat window to the bottom
    scrollChat: function($messageContainer, messageClass, sender){
        // When user scroll up in chat box, new incoming message wont bring it down.
        // Automatic scrolldown is only triggered if one of 3 condition is satisfied:
        // 1) if i'm already at the bottom of chat, 2) if message sender is me, and 3) if messages has not filled up
        // the chat yet and therefore scrollbar is still not visible (this condition is more of a bug fix)
        // To simply scroll down the chat, just dont put the parameters 'messageClass' or 'sender'
        if (!messageClass || !sender
            || $messageContainer.scrollTop() + $messageContainer.find(messageClass).last().outerHeight(true) == $messageContainer[0].scrollHeight - $messageContainer.outerHeight(true)
            || sender == myObj.username
            || $messageContainer[0].scrollHeight - $messageContainer.find(messageClass).last().outerHeight(true) <= $messageContainer.outerHeight(true) ){
            $messageContainer[0].scrollTop = $messageContainer[0].scrollHeight;
        }
    }

};

// set the height of an element to the sum of its children's height
$.fn.setActualHeight = function(){
    var previousCss  = this.attr("style");
    this.css({
        position:   'absolute',
        visibility: 'hidden',
        display:    'block'
    });
    var sum = 0;
    this.children().each(function(){
        sum += $(this).outerHeight(true);
    });
    this.attr("style", previousCss ? previousCss : "");
    this.height(sum);
};
