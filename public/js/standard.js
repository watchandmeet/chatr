$( document ).ready(function () {


    // ---------- helper animate.css functions ----------------
    $.fn.animatedShow = function(effect, callback, duration){ // duration can be "long" or "short", default is short
        var callback = _.isFunction(callback) ? callback : function(){};
        var durationClass = (duration == "long") ? "animate_duration_long" : "animate_duration_short";
        var classes = 'animated ' + durationClass + ' ' + effect;
        var $this = this;
        $this.addClass(classes).show()
        .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $this.removeClass(classes);
            callback();
        });
    }
    $.fn.animatedHide = function(effect, callback, duration){ // duration can be "long" or "short", default is short
        var callback = _.isFunction(callback) ? callback : function(){};
        var durationClass = (duration == "long") ? "animate_duration_long" : "animate_duration_short";
        var classes = 'animated ' + durationClass + ' ' + effect;
        var $this = this.data('uniqueId', new Date());
        $this.addClass(classes)
        .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $this.hide().removeClass(classes);
            callback();
        });
    }


    // =================================== slidable text (when too long) =================================== //

    // HOW TO USE:
    // 1) add class 'slidable' to text div
    // 2) text's parent div must have "overflow: hidden" to avoid text going out of bound
    // 3) text's parent div must have "width" set
    $(document)
    .on('mouseover', '.slidable', function(){
        $(this).stop(); // stop ongoing animation at current position
        $(this).data('originalMarginLeft', $(this).data('originalMarginLeft') || $(this).css('margin-left')); // store its initial margin-left value, used to restore to its initial position later
        var maxWidth = $(this).parent().width();
        var slidingDistance = $(this)[0].scrollWidth - maxWidth; // scrollWidth will give element's natural width
        var speed = 0.05; // sliding speed
        var time = slidingDistance / speed; // time it takes to slide
        if (slidingDistance > 0){
            $(this).animate(
                { 'margin-left':  -slidingDistance-parseInt($(this).css('text-indent')) },
                { duration: time, easing: 'swing' }
            );
        }
    })
    .on('mouseout', '.slidable', function(){
        $(this).stop(); // stop ongoing animation at current position
        $(this).animate(
            { 'margin-left' :  $(this).data('originalMarginLeft') },
            { duration: 400,  easing: 'swing' }
        );
    });




    // =================================== popup (middle) =================================== //

    // set each element to its correct height (sum of their children's height)
    $('.popup2').each(function(){
        $(this).setActualHeight();
    });

    // ----- mini plugin to display it -----
    $.fn.displayPopup2 = function(){
        $('.overlay').fadeIn(400);
        var input = this.find('input.popup2_input');
        // save each option button's initial state
        this.find('.popup2_option').each(function(){
            if (!$(this).data('data-text')) $(this).data('data-text', $(this).text());
        });
        this.animatedShow('zoomIn', function(){
            // if popup contains input area, focus cursor on that so user is ready to type
            if (input.length > 0){
                input.first().focus();
            }
        });
    };

    // close it
    $.fn.closePopup2 = function(callback){
        var callback = _.isFunction(callback) ? callback : function(){};
        // display hiding animation
        var duration = 400;
        var done = 0;
        var $this = this;
        function complete(){
            done++;
            if (done == 2) {
                // reset input and buttons' states to what they are initially
                var input = $this.find('input.popup2_input');
                input.prop("disabled", false).val('');
                $this.find('.popup2_option').each(function(){
                    $(this).text($(this).data('data-text'));
                });
                callback();
            }
        }
        this.animatedHide('zoomOut', complete);
        $('.overlay').fadeOut(duration, complete);
    };
    $(document).on('click', '.popup2_cancel', function(){
        $(this).closest('.popup2').closePopup2();
    });





    // =================================== popup (bottom) =================================== //

    // ----- mini plugin to display a popup -----
    $.fn.displayPopup = function(){
        this.show({ effect: 'slide', direction: 'down', duration: 120, easing: 'linear' });
        $('.overlay').fadeIn(120);
    };

    // close the popup
    $(document).on('click', '.popup_cancel, .popup_option', function(){
        // display hiding animation
        var $popup = $(this).closest('.popup');
        var duration = 120;
        $popup.hide({ effect: 'slide', direction: 'down', duration: duration, easing: 'linear' });
        $('.overlay').fadeOut(duration);
    });



    // ------------------------------------- pop (middle & full screen) ------------------------------------- //

    // ----- mini plugin to display pop -----
    $.fn.displayPop = function(){
        $('.overlay_big').fadeIn(400);
        $(this).animatedShow('zoomIn', null, 'long');
    };

    var hideOverlay = function($popup){
        var level = $popup.attr('level');
        if (level == 2){
            $('.overlay_big2').fadeOut(400);
        } else {
            $('.overlay_big').fadeOut(400);
        }
    };
    $(document).keyup(function(e) { // when press esc
        // attribute 'safe' is false means pop is only closed when click on right corner 'x'
        if (e.keyCode == 27) {
            $('.pop').each(function(){
                if ($(this).is(':visible') && $(this).attr('safe')!='false'){
                    hideOverlay($(this));
                    $(this).hide();
                }
            });
        }
    }).mouseup(function(e){ // when click outside of pop
        $('.pop').each(function(){
            if ( !$(this).is(e.target) && !$(this).has(e.target).length && $(this).is(":visible") && $(this).attr('safe')!='false'){
                hideOverlay($(this));
                $(this).hide();
            }
        });
    });
    $(document).on('click', '.pop_close, .pop_cancel', function(){
        $pop = $(this).closest('.pop');
        $pop.animatedHide('fadeOut');
        hideOverlay($pop);
    });







});